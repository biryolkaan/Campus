const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);


// Add an event to user_events
exports.set_user_events = functions.database.ref('social/events/{pushId}')
    .onWrite((event) => {
        const original = event.data.val();
        const uid = event.data.child('owner').child('uid').val();
        console.log('set_user_events_value', event.data.child('upvoters').val());

        const user_event_promise = admin.database().ref('social/user_events/' + uid + "/" + event.params.pushId + "/").set(original);
        const followers_promise = user_event_promise.then(snap => {
            return admin.database().ref('social/users/' + uid + '/followers/').once('value').then(function(snapshot) {
                return snapshot.forEach(function(child) {
                    console.log("writing to timeline: ", child.key);
                    admin.database().ref('social/timeline/' + child.key + '/' + event.params.pushId).set(true);
                });
            }).then(snap => {
                const uid = event.data.child('tags').val();
                for (key in uid) {
                    admin.database().ref('social/search/' + uid[key] + '/' + event.params.pushId).set(true);
                }
            });
        });
        return followers_promise;
    });

exports.sendFollowerNotification = functions.database.ref('social/users/{followedUid}/followers/{followerUid}').onWrite(event => {
    const followerUid = event.params.followerUid;
    const followedUid = event.params.followedUid;
    // If un-follow we exit the function.
    if (!event.data.val()) {
        return console.log('User ', followerUid, 'un-followed user', followedUid);
    }
    console.log('We have a new follower UID:', followerUid, 'for user:', followerUid);

    // Get the list of device notification tokens.
    const getDeviceTokensPromise = admin.database().ref(`social/users/${followedUid}/notificationTokens`).once('value');

    // Get the follower profile.
    const getFollowerProfilePromise = admin.auth().getUser(followerUid);

    return Promise.all([getDeviceTokensPromise, getFollowerProfilePromise]).then(results => {
        const tokensSnapshot = results[0];
        const follower = results[1];

        // Check if there are any device tokens.
        if (!tokensSnapshot.hasChildren()) {
            return console.log('There are no notification tokens to send to.');
        }
        console.log('There are', tokensSnapshot.numChildren(), 'tokens to send notifications to.');
        console.log('Fetched follower profile', follower);

        // Notification details.
        const payload = {
            notification: {
                title: 'You have a new follower!',
                body: `${follower.displayName} is now following you.`,
                sound: 'default',
                tag: 'follower',
                click_action: '.ui.user_profile.view_interaction.ViewInteractionActivity'
            }
        };

        // Listing all tokens.
        const tokens = Object.keys(tokensSnapshot.val());

        // Send notifications to all tokens.
        return admin.messaging().sendToDevice(tokens, payload).then(response => {
            // For each message check if there was an error.
            const tokensToRemove = [];
            response.results.forEach((result, index) => {
                const error = result.error;
                if (error) {
                    console.error('Failure sending notification to', tokens[index], error);
                    // Cleanup the tokens who are not registered anymore.
                    if (error.code === 'messaging/invalid-registration-token' ||
                        error.code === 'messaging/registration-token-not-registered') {
                        tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
                    }
                }
            });
            return Promise.all(tokensToRemove);
        });
    });
});

exports.sendChatNotification = functions.database.ref('social/chats/{chatId}/{uid}/{messageId}').onWrite(event => {

    const messageSender = event.data.child('owner').child('uid').val();
    const messageReceiver = event.data.child('receiverId').val();
    const message = event.data.child('message').val();


    console.log('Notification for message LOG : sender', messageSender, 'for user:', messageReceiver, 'message : ', message);
    // If un-follow we exit the function.
    if (!event.data.val() || messageReceiver == event.params.uid) {
        return console.log('Message notification ', messageSender, 'exit', messageReceiver);
    }
    console.log('We have a new chat message:', messageSender, 'for user:', messageReceiver);

    // Get the list of device notification tokens.
    const getDeviceTokensPromise = admin.database().ref(`social/users/${messageReceiver}/notificationTokens`).once('value');

    // Get the follower profile.
    const getFollowerProfilePromise = admin.auth().getUser(messageSender);

    return Promise.all([getDeviceTokensPromise, getFollowerProfilePromise]).then(results => {
        const tokensSnapshot = results[0];
        const follower = results[1];

        // Check if there are any device tokens.
        if (!tokensSnapshot.hasChildren()) {
            return console.log('There are no notification tokens to send to.');
        }
        console.log('There are', tokensSnapshot.numChildren(), 'tokens to send notifications to.');
        console.log('Fetched follower profile', follower);

        // Notification details.
        const payload = {
            notification: {
                title: `New message from : ${follower.displayName}`,
                body: message,
                sound: 'default',
                tag: 'chat'

            }
        };

        // Listing all tokens.
        const tokens = Object.keys(tokensSnapshot.val());

        // Send notifications to all tokens.
        return admin.messaging().sendToDevice(tokens, payload).then(response => {
            // For each message check if there was an error.
            const tokensToRemove = [];
            response.results.forEach((result, index) => {
                const error = result.error;
                if (error) {
                    console.error('Failure sending notification to', tokens[index], error);
                    // Cleanup the tokens who are not registered anymore.
                    if (error.code === 'messaging/invalid-registration-token' ||
                        error.code === 'messaging/registration-token-not-registered') {
                        tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
                    }
                }
            });
            return Promise.all(tokensToRemove);
        });
    });
});


exports.update_owner_info = functions.database.ref('social/users/{userId}/')
    .onWrite((event) => {
        const original = event.data.val();
        const avatar_url = event.data.child('avatar_url').val();
        const bio = event.data.child('bio').val();
        const name = event.data.child('name').val();
        const surname = event.data.child('surname').val();
        const uid = event.data.child('uid').val();
        var data = {
            avatar_url: avatar_url,
            bio: bio,
            name: name,
            surname: surname,
            uid: uid
        };

        var data_comment = {
            avatar_url: avatar_url,
            name: name,
            surname: surname,
            uid: uid
        };
        console.log('avatar_url', original);


        return admin.database().ref('social/user_events/' + event.params.userId).once('value').then(function(snapshot) {
                let updateObj = {};
                return snapshot.forEach(function(child) {
                    //    updateObj['social/user_events/'+ event.params.userId + '/' + child.key  + '/owner/avatar_url'] = original;
                    admin.database().ref('social/user_events/' + event.params.userId + '/' + child.key + '/owner/').set(data);
                    admin.database().ref('social/events/' + child.key + '/owner/').set(data);
                });
            })
            .then(snap => {
                return admin.database().ref('social/user_comments/' + event.params.userId).once('value').then(function(snapshot) {
                    return snapshot.forEach(function(child) {
                        admin.database().ref('social/comments/' + child.val() + '/' + child.key + '/owner/').set(data_comment);
                    });
                });
            });
    });


exports.create_timeline = functions.database.ref("social/users/{userId}/following/{followingId}").onWrite((event) => {

    var collectionRef = event.data.ref.parent.parent;
    var countRef = collectionRef.child('following_count');

    return countRef.transaction(current => {
        if (event.data.exists() && !event.data.previous.exists()) {
            return (current || 0) + 1;
        } else if (!event.data.exists() && event.data.previous.exists()) {
            return (current || 0) - 1;
        }
    }).then(() => {
        console.log('Count updated.');
        return admin.database().ref('social/user_events/' + event.params.followingId).once('value').then(function(snapshot) {
            return snapshot.forEach(function(child) {
                if (event.data.previous.exists()) {
                    admin.database().ref('social/timeline/' + event.params.userId + '/' + child.key).remove();
                } else {
                    admin.database().ref('social/timeline/' + event.params.userId + '/' + child.key).set(true);
                }

            });
        });
    });
});

// Keep track of followers
exports.count_followers = functions.database.ref("social/users/{userId}/followers/{$followerId}").onWrite((event) => {
    const collectionRef = event.data.ref.parent.parent;
    const countRef = collectionRef.child('follower_count');

    return countRef.transaction(current => {

        if (event.data.exists() && !event.data.previous.exists()) {

            return (current || 0) + 1;
        } else if (!event.data.exists() && event.data.previous.exists()) {

            return (current || 0) - 1;
        }

    }).then(() => {
        console.log('Count updated.');
    });
});

// If the follewer_count gets deleted recount
exports.recount_followers = functions.database.ref("social/users/{userId}/follower_count/").onWrite((event) => {
    if (!event.data.exists()) {
        const counterRef = event.data.ref;
        const collectionRef = counterRef.parent.child('followers');
        return collectionRef.once('value')
            .then(messagesData => counterRef.set(messagesData.numChildren()));
    }
});



// If the following_count gets deleted recount
exports.recount_following = functions.database.ref("social/users/{userId}/following_count/").onWrite((event) => {
    if (!event.data.exists()) {
        var counterRef = event.data.ref;
        var collectionRef = counterRef.parent.child('following');
        return collectionRef.once('value')
            .then(messagesData => counterRef.set(messagesData.numChildren()));
    }

    if (!event.data.exists()) {
        const counterRef = event.data.ref;
        const collectionRef = counterRef.parent.child('followers');
        return collectionRef.once('value')
            .then(messagesData => counterRef.set(messagesData.numChildren()));
    }
});

// If the following_count gets deleted recount
exports.count_comment_rating = functions.database.ref("social/comments/{eventId}/{commentId}/votes/{$userId}").onWrite((event) => {
    const collectionRef = event.data.ref.parent;
    const countRef = collectionRef.parent.child('rating');
    var total = 0;
    return collectionRef.once('value').then(function(snapshot) {
        return snapshot.forEach(function(child) {
            total += child.val();
        });
    }).then(() => {
        countRef.set(total);
    });
});

// set latest message
exports.set_latest_message = functions.database.ref('social/chats/{chatId}/{uid}/{messageId}')
    .onWrite((event) => {
        const original = event.data.val();
        const uid = event.data.child('owner').child('uid').val();
        const receiverId = event.data.child('receiverId').val();
        const message = event.data.child('message').val();
        console.log('set_latest_message', message);
        console.log('receiverId', receiverId);
        var mergedUpdate = {};


        mergedUpdate[uid + "/" + event.params.chatId + "/latestMessage"] = message;
        mergedUpdate[receiverId + "/" + event.params.chatId + "/latestMessage"] = message;
        //  mergedUpdate[receiverId + "/" + event.params.chatId + "/isSeen" ] = false;
        return admin.database().ref('social/user_chats/').update(mergedUpdate);
    });


exports.get_similar_users = functions.https.onRequest((req, res) => {
    const promises = [];
    const userId = req.query.uid;
    const interests = req.query.interests;
    var alreadyFollowing = req.query.following.split("_");
    return admin.database().ref('social/users/').once('value').then(function(snapshot) {
        return snapshot.forEach(function(child) {

            var avatar_url = child.child('avatar_url').val();
            var name = child.child('name').val();
            var surname = child.child('surname').val();
            var uid = child.child('uid').val();
            console.log('%%%%', similarity(interests, child.child('interests').val()));
            var simValue = similarity(interests, child.child('interests').val());
            var sim = "%" + (simValue.toFixed(2) * 100).toString();
            var data = {
                avatar_url: avatar_url,
                name: name,
                surname: surname,
                uid: uid,
                similarity: sim
            };
            if (!(alreadyFollowing.indexOf(uid) > -1)) {
                admin.database().ref('social/user_suggestions/' + userId + "/" + uid).set(data);
            }
        });
    }).then(snapshot => {
        res.status(200).send("success");
    });;
});

// Keep track of following
exports.count_comments = functions.database.ref("social/comments/{eventId}/{commentId}").onWrite((event) => {

    var countRef = admin.database().ref('social/events/' + event.params.eventId + "/comment_count");
    return countRef.transaction(current => {
        if (event.data.exists() && !event.data.previous.exists()) {
            return (current || 0) + 1;
        } else if (!event.data.exists() && event.data.previous.exists()) {
            return (current || 0) - 1;
        }
    }).then(() => {
        console.log('Count updated.');
    });
});

// If the following_count gets deleted recount
exports.recount_comments = functions.database.ref("social/events/{eventId}/comment_count/").onWrite((event) => {
    if (!event.data.exists()) {
        var counterRef = event.data.ref;
        var collectionRef = admin.database().ref('social/comments/' + event.params.eventId);
        return collectionRef.once('value')
            .then(messagesData => counterRef.set(messagesData.numChildren()));
    }
});


function similarity(s1, s2) {
    var longer = s1;
    var shorter = s2;
    if (s1.length < s2.length) {
        longer = s2;
        shorter = s1;
    }
    var longerLength = longer.length;
    if (longerLength == 0) {
        return 1.0;
    }
    return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
}

function editDistance(s1, s2) {
    s1 = s1.toLowerCase();
    s2 = s2.toLowerCase();

    var costs = new Array();
    for (var i = 0; i <= s1.length; i++) {
        var lastValue = i;
        for (var j = 0; j <= s2.length; j++) {
            if (i == 0)
                costs[j] = j;
            else {
                if (j > 0) {
                    var newValue = costs[j - 1];
                    if (s1.charAt(i - 1) != s2.charAt(j - 1))
                        newValue = Math.min(Math.min(newValue, lastValue),
                            costs[j]) + 1;
                    costs[j - 1] = lastValue;
                    lastValue = newValue;
                }
            }
        }
        if (i > 0)
            costs[s2.length] = lastValue;
    }
    return costs[s2.length];
}
