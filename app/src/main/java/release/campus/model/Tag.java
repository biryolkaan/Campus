package release.campus.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Morgoth on 25.2.2017.
 */
@IgnoreExtraProperties
public class Tag {

    public String key;
    public String tag;
    public boolean isSelected;


    public Tag() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Tag(String key, String tag, boolean b) {
        this.key = key;
        this.tag = tag;
        this.isSelected = b;
    }

    @Exclude
    public boolean isSelected() {
        return isSelected;
    }

    @Exclude
    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Tag(String key, String tag) {
        this.key = key;
        this.tag = tag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return getTag();
    }
}
