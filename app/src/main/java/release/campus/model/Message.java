package release.campus.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Morgoth on 15.4.2017.
 */

public class Message {

    public String receiverId;
    public String message;
    public Owner owner;
    @Exclude
    public boolean isMyMessage;

    public Message() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Message(String receiver, String m) {
        this.message = m;
        this.receiverId = receiver;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    @Exclude
    public boolean isMyMessage() {
        return isMyMessage;
    }
    @Exclude
    public void setMyMessage(boolean myMessage) {
        isMyMessage = myMessage;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("receiverId", receiverId);
        result.put("message", message);
        result.put("owner", owner);

        return result;
    }
}
