package release.campus.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.Map;

/**
 * Created by Morgoth on 7.4.2017.
 */

@IgnoreExtraProperties
public class Comment {

    public String commentId;
    public String eventId;
    public String content;
    public Owner owner;
    public Map<String, Long> votes;
    public long rating;
    @Exclude
    public boolean isUpvotedByCurrentUser;
    @Exclude
    public boolean isDownvotedByCurrentUser;

    public Comment() {

    }
    @Exclude
    public boolean isDownvotedByCurrentUser() {
        return isDownvotedByCurrentUser;
    }
    @Exclude
    public void setDownvotedByCurrentUser(boolean downvotedByCurrentUser) {
        isDownvotedByCurrentUser = downvotedByCurrentUser;
    }

    public long getRating() {
        return rating;
    }

    @Exclude
    public boolean isUpvotedByCurrentUser() {
        return isUpvotedByCurrentUser;
    }

    @Exclude
    public void setUpvotedByCurrentUser(boolean upvotedByCurrentUser) {
        isUpvotedByCurrentUser = upvotedByCurrentUser;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Map<String, Long> getVotes() {
        return votes;
    }

    public void setVotes(Map<String, Long> votes) {
        this.votes = votes;
    }
}
