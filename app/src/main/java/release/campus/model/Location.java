package release.campus.model;

/**
 * Created by Morgoth on 20.5.2017.
 */

public class Location {

    private double lat;
    private double longi;
    private String id  ;
    private String name ;

    public Location() {

    }

    public Location(double lat, double longi, String id, String name) {
        this.lat = lat;
        this.longi = longi;
        this.id = id;
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
