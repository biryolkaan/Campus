package release.campus.model;

import com.google.firebase.database.IgnoreExtraProperties;

import org.parceler.Parcel;

/**
 * Created by Morgoth on 14.3.2017.
 */

@IgnoreExtraProperties
@Parcel
public class Owner {

    public String uid;
    public String name;
    public String surname;
    public String bio;
    public String avatar_url;

    public Owner() {
    }

    public Owner(String uid, String name, String surname, String bio, String avatar_url) {
        this.uid = uid;
        this.name = name;
        this.surname = surname;
        this.bio = bio;
        this.avatar_url = avatar_url;
    }

    public Owner(String name, String surname, String avatar_url) {
        this.name = name;
        this.surname = surname;
        this.avatar_url = avatar_url;
    }

    public Owner(String uid, String name, String surname, String avatar_url) {
        this.uid = uid;
        this.name = name;
        this.surname = surname;
        this.avatar_url = avatar_url;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}

