package release.campus.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Morgoth on 24.2.2017.
 */

@IgnoreExtraProperties
public class User {

    public String uid;
    public String email;
    public String name;
    public String bio;
    public String surname;
    public String interests;
    public String avatar_url;
    public long following_count;
    public long follower_count;
    public long event_count;
    public String school;
    public Map<String, Boolean> followers = new HashMap<>();
    public Map<String, Boolean> following = new HashMap<>();
    public Map<String, Object> events = new HashMap<>();
    @Exclude
    public boolean isFollowing;
    public String similarity;
    public Map<String, Boolean> notificationTokens;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Map<String, Boolean> getNotificationTokens() {
        return notificationTokens;
    }

    public void setNotificationTokens(Map<String, Boolean> notificationToken) {
        this.notificationTokens = notificationToken;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getSimilarity() {
        return similarity;
    }

    public void setSimilarity(String similarity) {
        this.similarity = similarity;
    }

    public User(String email, String name, String surname, String avatar_url, long follower, long following, String uid) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.avatar_url = avatar_url;
        this.following_count = following;
        this.follower_count = follower;
        this.uid = uid;
    }

    public User(String email,String name, String surname,String interest) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.interests = interest;
    }


    public User(String email,String name, String surname) {
        this.email = email;
        this.name = name;
        this.surname = surname;
    }


    @Exclude
    public boolean isFollowing() {
        return isFollowing;
    }

    @Exclude
    public void setFollowing(boolean following) {
        isFollowing = following;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public long getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(long following_count) {
        this.following_count = following_count;
    }

    public long getFollower_count() {
        return follower_count;
    }

    public void setFollower_count(long follower_count) {
        this.follower_count = follower_count;
    }

    public Map<String, Boolean> getFollowing() {
        return following;
    }

    public void setFollowing(Map<String, Boolean> following) {
        this.following = following;
    }

    public Map<String, Boolean> getFollowers() {
        return followers;
    }

    public void setFollowers(Map<String, Boolean> followers) {
        this.followers = followers;
    }

    public Map<String, Object> getEvents() {
        return events;
    }

    public void setEvents(Map<String, Object> events) {
        this.events = events;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public long getEvent_count() {
        return event_count;
    }

    public void setEvent_count(long event_count) {
        this.event_count = event_count;
    }
}