package release.campus.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import org.parceler.Parcel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Morgoth on 4.3.2017.
 */

@IgnoreExtraProperties
@Parcel
public class Event{

    public String eventId;
    public String title;
    public List<String> tags;
    public String message;
    public Long rating;
    public Owner owner;
    public Long timestamp;
    public double latitude;
    public double longitude;
    public String locationId;
    public String locationName;
    public long eventTime;
    @Exclude
    public Map<String, String> dateCreated;
    public Map<String, Boolean> upvoters;
    @Exclude
    public boolean isUpvotedByCurrentUser;
    public long comment_count;
    public Event() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Event(String title, List<String> tags, String message, Long rating, Owner u) {
        this.title = title;
        this.tags = tags;
        this.message = message;
        this.rating = rating;
        this.owner = u;
    }


    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public long getComment_count() {
        return comment_count;
    }

    public void setComment_count(long comment_count) {
        this.comment_count = comment_count;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getEventTime() {
        return eventTime;
    }

    public void setEventTime(long eventTime) {
        this.eventTime = eventTime;
    }

    public Event(String key, String title, List<String> tags, String message, long rating, Owner owner,
                 Map<String, String> dateCreated, double lati, double longi, String locationId, String locationName, long eventTime) {
        this.eventId = key;
        this.title = title;
        this.tags = tags;
        this.message = message;
        this.rating = rating;
        this.owner = owner;
        this.dateCreated = dateCreated;
        this.latitude = lati;
        this.longitude = longi;
        this.locationId = locationId;
        this.locationName = locationName;
        this.eventTime = eventTime;
    }


    public Map<String, Boolean> getUpvoters() {
        return upvoters;
    }

    public void setUpvoters(Map<String, Boolean> upvoters) {
        this.upvoters = upvoters;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Exclude
    public void setDateCreated(Map<String, String> dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Exclude
    public long getDateCreated() {
        return timestamp;
    }

    public Event(String title, List<String> tags, String message, Long rating, Owner u, Map<String,String> dateCreated) {
        this.title = title;
        this.tags = tags;
        this.message = message;
        this.rating = rating;
        this.owner = u;
        this.dateCreated = dateCreated;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @Exclude
    public boolean isUpvotedByCurrentUser() {
        return isUpvotedByCurrentUser;
    }

    @Exclude
    public void setUpvotedByCurrentUser(boolean upvotedByCurrentUser) {
        isUpvotedByCurrentUser = upvotedByCurrentUser;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("eventId", eventId);
        result.put("title", title);
        result.put("tags", tags);
        result.put("message", message);
        result.put("rating", rating);
        result.put("owner", owner);
        result.put("timestamp", dateCreated);
        result.put("upvoters", upvoters);
        result.put("latitude", latitude);
        result.put("longitude", longitude);
        result.put("locationId", locationId);
        result.put("locationName", locationName);
        result.put("eventTime", eventTime);

        return result;
    }

    @Exclude
    public Map<String, Object> toMap_user_events() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("title", title);
        result.put("tags", tags);
        result.put("message", message);
        result.put("rating", rating);
        result.put("owner", owner);
        result.put("timestamp", dateCreated);
        return result;
    }

    @Exclude
    public boolean isEmpty() {
        if(title==null && tags==null && message ==null && rating==null && owner==null && timestamp==null)
            return true;
        return false;
    }
}
