package release.campus.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import org.parceler.Parcel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Morgoth on 15.4.2017.
 */
@IgnoreExtraProperties
@Parcel
public class Chat {

    public String chatId;
    public String chatOwnerId;
    public String chatReceiverId;
    public String chatOwnerName;
    public String chatReceiverName;
    public String chatOwnerAvatar;
    public String chatReceiverAvatar;
    public String latestMessage;

    public Chat() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Chat(String chatId, String chatOwnerId, String chatReceiverId, String chatOwnerName, String chatReceiverName,
                String chatOwnerAvatar, String chatReceiverAvatar) {
        this.chatId = chatId;
        this.chatOwnerId = chatOwnerId;
        this.chatReceiverId = chatReceiverId;
        this.chatOwnerName  = chatOwnerName;
        this.chatReceiverName = chatReceiverName;
        this.chatOwnerAvatar = chatOwnerAvatar;
        this.chatReceiverAvatar = chatReceiverAvatar;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getLatestMessage() {
        return latestMessage;
    }

    public void setLatestMessage(String latestMessage) {
        this.latestMessage = latestMessage;
    }

    public String getChatOwnerAvatar() {
        return chatOwnerAvatar;
    }

    public void setChatOwnerAvatar(String chatOwnerAvatar) {
        this.chatOwnerAvatar = chatOwnerAvatar;
    }

    public String getChatReceiverAvatar() {
        return chatReceiverAvatar;
    }

    public void setChatReceiverAvatar(String chatReceiverAvatar) {
        this.chatReceiverAvatar = chatReceiverAvatar;
    }

    public String getChatOwnerId() {
        return chatOwnerId;
    }

    public void setChatOwnerId(String chatOwnerId) {
        this.chatOwnerId = chatOwnerId;
    }

    public String getChatReceiverId() {
        return chatReceiverId;
    }

    public void setChatReceiverId(String chatReceiverId) {
        this.chatReceiverId = chatReceiverId;
    }

    public String getChatOwnerName() {
        return chatOwnerName;
    }

    public void setChatOwnerName(String chatOwnerName) {
        this.chatOwnerName = chatOwnerName;
    }

    public String getChatReceiverName() {
        return chatReceiverName;
    }

    public void setChatReceiverName(String chatReceiverName) {
        this.chatReceiverName = chatReceiverName;
    }

    @Exclude
    public Map<String, Object> toMap() {

        HashMap<String, Object> result = new HashMap<>();
        result.put("chatId", chatId);
        result.put("chatOwnerId", chatOwnerId);
        result.put("chatReceiverId", chatReceiverId);
        result.put("chatOwnerName", chatOwnerName);
        result.put("chatReceiverName", chatReceiverName);
        result.put("chatOwnerAvatar", chatOwnerAvatar);
        result.put("chatReceiverAvatar", chatReceiverAvatar);
        result.put("latestMessage", latestMessage);

        return result;
    }


}
