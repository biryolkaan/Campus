package release.campus.ui.user_profile;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.ActivityModule;
import release.campus.di.scopes.ActivityScope;
import release.campus.ui.base.profile.UserEventsAdapter;

/**
 * Created by Morgoth on 11.3.2017.
 */
@Module
public class UserProfileActivityModule extends ActivityModule {

    public UserProfileActivityModule(AppCompatActivity activity) {
        super(activity);
    }

    @Provides
    @ActivityScope
    public UserProfileActivityPresenter provideUserProfileActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase firebaseDatabase) {
        return new UserProfileActivityPresenter(firebaseAuth,firebaseDatabase);
    }

    @Provides
    @ActivityScope
    public UserEventsAdapter provideUserEventsAdapter(Context context,UserProfileActivityPresenter userProfileActivityPresenter) {
        return new UserEventsAdapter(context,userProfileActivityPresenter);
    }
}
