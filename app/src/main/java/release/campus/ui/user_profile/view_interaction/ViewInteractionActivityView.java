package release.campus.ui.user_profile.view_interaction;

import java.util.List;

import release.campus.model.User;

/**
 * Created by Morgoth on 29.5.2017.
 */

public interface ViewInteractionActivityView {
    void onUsersSet(List<User> userList);
}
