package release.campus.ui.user_profile.view_interaction;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import release.campus.R;
import release.campus.model.Owner;
import release.campus.model.User;
import release.campus.ui.user_profile.UserProfileActivity;

/**
 * Created by Morgoth on 29.5.2017.
 */

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private Context context;
    private List<User> userList;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.root_view)
        ConstraintLayout layout;
        @BindView(R.id.profile_picture)
        CircularImageView avatar;
        @BindView(R.id.tv_username)
        TextView name;
        @BindView(R.id.tv_bio)
        TextView bio;

        private User user;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }

        public void bindData(User user) {
            this.user = user;
        }

        @OnClick(R.id.root_view)
        void setLayouOnClick() {
            Context context = avatar.getContext();
            Intent i = new Intent(context, UserProfileActivity.class);
            i.putExtra("uid",user.getUid());
            Owner owner = new Owner(user.getUid(), user.getName(), user.getSurname(), user.getBio(), user.getAvatar_url());
            i.putExtra("user", Parcels.wrap(owner));
            context.startActivity(i);

        }
    }

    public UsersAdapter(Context context) {
        this.context = context;

    }

    @Override
    public UsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_users, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        //todo util
        holder.name.setText(userList.get(position).getName() + " " + userList.get(position).getSurname());

        holder.bio.setText(userList.get(position).getBio());

        Glide.with(context)
                .load(userList.get(position).getAvatar_url())
                .centerCrop()
                .crossFade()
                .into(holder.avatar);
        holder.bindData(userList.get(position));
    }

    public void update(List<User> list) {
        this.userList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (userList !=null) ? userList.size() : 0 ;
    }

}