package release.campus.ui.user_profile;

import java.util.List;

import release.campus.model.Event;
import release.campus.model.User;

/**
 * Created by Morgoth on 11.3.2017.
 */

public interface UserProfileActivityView {
    void setInfo(User u);

    void setEvents(List<Event> eventList, int previousSize, int size);

    void setRefreshing();

    void onFollowComplete(boolean followed);
}
