package release.campus.ui.user_profile.view_interaction;

import dagger.Subcomponent;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 29.5.2017.
 */

@ActivityScope
@Subcomponent(
        modules = {
                ViewInteractionActivityModule.class
        }
)
public interface ViewInteractionActivityComponent {
    void injectTo(ViewInteractionActivity activity);
}

