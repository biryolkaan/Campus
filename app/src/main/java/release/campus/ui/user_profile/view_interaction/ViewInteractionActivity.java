package release.campus.ui.user_profile.view_interaction;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Event;
import release.campus.model.User;
import release.campus.ui.BaseActivity;

public class ViewInteractionActivity extends BaseActivity implements ViewInteractionActivityView{


    @Inject ViewInteractionActivityPresenter presenter;
    @Inject UsersAdapter adapter;

    @BindView(R.id.rv_users)
    RecyclerView recyclerView;
    @BindView(R.id.progress_overlay)
    FrameLayout progress_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
        setContentView(R.layout.activity_view_interaction);
        setupRecyclerView();
        boolean interaction = getIntent().getBooleanExtra("interaction",true);
        boolean rating = getIntent().getBooleanExtra("rating",false);
        if (rating) {
            getEventFromIntent();
            setTitle(getString(R.string.title_likes));
        }
        else if(interaction) {
            presenter.getFollowers();
            setTitle(getString(R.string.title_followers));
        }
        else{
            presenter.getFollowing();
            setTitle(getString(R.string.title_following));
        }
    }

    private void getEventFromIntent() {
        Event event = (Event) Parcels.unwrap(getIntent().getParcelableExtra("event"));
        presenter.getLikes(event.getUpvoters());
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new ViewInteractionActivityModule(this)).injectTo(this);
    }

    @Override
    public void onUsersSet(List<User> userList) {
        adapter.update(userList);
        progress_bar.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
