package release.campus.ui.user_profile.view_interaction;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import release.campus.model.User;
import release.campus.presenter.Presenter;
import release.campus.utils.CompositeReference;
import release.campus.utils.FirebaseListenerReference;

/**
 * Created by Morgoth on 29.5.2017.
 */

public class ViewInteractionActivityPresenter extends Presenter<ViewInteractionActivityView> {

    private CompositeReference compositeReference;
    private DatabaseReference rootRef;
    private FirebaseUser currentUser;
    private List<User> userList;

    public ViewInteractionActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase firebaseDatabase) {
        this.currentUser = firebaseAuth.getCurrentUser();
        this.rootRef = firebaseDatabase.getReference().child("social");
    }

    void getFollowers() {

        DatabaseReference followersRef = rootRef.child("users").child(currentUser.getUid());

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                User u = dataSnapshot.getValue(User.class);
                for (String uid: u.getFollowers().keySet()) {
                    Query query = rootRef.child("users").child(uid);
                    ValueEventListener valueEventListener1 = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User u = dataSnapshot.getValue(User.class);
                            userList.add(u);
                            if(view!=null)
                                view.onUsersSet(userList);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    };
                    compositeReference.add(new FirebaseListenerReference(query,valueEventListener1,true));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        compositeReference.add(new FirebaseListenerReference(followersRef,valueEventListener,true));

    }


    void getFollowing() {

        DatabaseReference followersRef = rootRef.child("users").child(currentUser.getUid());

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                User u = dataSnapshot.getValue(User.class);
                for (String uid: u.getFollowing().keySet()) {
                    Query query = rootRef.child("users").child(uid);
                    ValueEventListener valueEventListener1 = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User u = dataSnapshot.getValue(User.class);
                            userList.add(u);
                            if(view!=null)
                                view.onUsersSet(userList);

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    };
                    compositeReference.add(new FirebaseListenerReference(query,valueEventListener1,true));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        compositeReference.add(new FirebaseListenerReference(followersRef,valueEventListener,true));

    }

    void getLikes(Map<String, Boolean> upvoters) {
        for (String uid : upvoters.keySet()) {
            DatabaseReference followersRef = rootRef.child("users").child(uid);

            ValueEventListener valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User u = dataSnapshot.getValue(User.class);
                    userList.add(u);
                    view.onUsersSet(userList);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            compositeReference.add(new FirebaseListenerReference(followersRef, valueEventListener, true));
        }
    }

    @Override
    public void onViewDetached() {
        super.onViewDetached();
    }

    @Override
    public void onViewAttached(ViewInteractionActivityView view) {
        super.onViewAttached(view);
        userList = new ArrayList<>();
        compositeReference = new CompositeReference();
    }
}
