package release.campus.ui.user_profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Event;
import release.campus.model.Owner;
import release.campus.model.User;
import release.campus.ui.BaseActivity;
import release.campus.ui.base.profile.UserEventsAdapter;
import release.campus.ui.chat.ChatActivity;
import release.campus.utils.FeedItemAnimator;
import release.campus.utils.ValidationUtils;

/**
 * Created by Morgoth on 11.3.2017.
 */

public class UserProfileActivity extends BaseActivity implements UserProfileActivityView, SwipeRefreshLayout.OnRefreshListener {

    @Inject UserProfileActivityPresenter presenter;
    @Inject UserEventsAdapter adapter;
    @Inject FirebaseAuth mAuth;

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.bio) TextView bio;
    @BindView(R.id.rv_user_events) RecyclerView recyclerView;
    @BindView(R.id.profile_picture) CircularImageView avatar;
    @BindView(R.id.follow) ImageButton follow;
    @BindView(R.id.followers) TextView followers;
    @BindView(R.id.following) TextView following;
    @BindView(R.id.progress_overlay) FrameLayout progress_bar;
    @BindView(R.id.fab_new_chat)
    FloatingActionButton fabChat;

    private String uid = "";
    private Owner user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
        setContentView(R.layout.activity_user_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupRecyclerView();
        setupSwipeRefreshLayout();
        setUserInfo();
        presenter.getProfileInfo(uid);
    }

    private void setUserInfo() {
        Intent i = getIntent();
        uid = i.getStringExtra("uid");
        user = (Owner) Parcels.unwrap(getIntent().getParcelableExtra("user"));
        name.setText(user.getName() + " " + user.getSurname());
        bio.setText(user.getBio());
        Glide.with(this)
                .load(user.getAvatar_url())
                .centerCrop()
                .crossFade()
                .into(avatar);
        getSupportActionBar().setTitle(user.getName() + " " + user.getSurname());
        if(!mAuth.getCurrentUser().getUid().equals(uid)){
            fabChat.setVisibility(View.VISIBLE);
            follow.setVisibility(View.VISIBLE);
        }

    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setItemAnimator(new FeedItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new UserProfileActivityModule(this)).injectTo(this);
    }

    @Override
    protected void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }

    @Override
    public void setInfo(User u) {
        if(u.isFollowing())
            follow.setImageDrawable(getDrawable(R.drawable.account_minus));
        else follow.setImageDrawable(getDrawable(R.drawable.account_plus));
        followers.setText(ValidationUtils.convertWithSuffix(u.getFollower_count()));
        following.setText(ValidationUtils.convertWithSuffix(u.getFollowing_count()));
        progress_bar.setVisibility(View.GONE);

    }

    @OnClick(R.id.follow)
    void setOnClickFollow() {
        presenter.followAndUnFollow(uid);
    }

    @Override
    public void setEvents(List<Event> eventList, int previousSize, int size) {
        swipeRefreshLayout.setRefreshing(false);
        adapter.update(eventList, previousSize, size);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab_new_chat)
    void setOnClickChat() {
        Intent i = new Intent(this, ChatActivity.class);
        i.putExtra("uid",uid);
        i.putExtra("receiver", Parcels.wrap(user));
        startActivity(i);
    }

    @Override
    public void setRefreshing() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onFollowComplete(boolean followed) {
        try{
            long value = Long.parseLong(followers.getText().toString());
            if(value<1000) {
                if (followed)
                    value++;
                else if (!followed)
                    value--;
                followers.setText(ValidationUtils.convertWithSuffix(value));
            }
        }catch(Exception excepciton) {
            Log.e("VALUE",excepciton.getMessage().toString());
        }
    }

    @Override
    public void onRefresh() {
        adapter.notifyRemoved();
        presenter.getProfileInfo(uid);
    }
}
