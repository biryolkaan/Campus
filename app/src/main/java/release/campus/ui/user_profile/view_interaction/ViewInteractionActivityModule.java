package release.campus.ui.user_profile.view_interaction;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.ActivityModule;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 29.5.2017.
 */
@Module
public class ViewInteractionActivityModule extends ActivityModule {

    public ViewInteractionActivityModule(AppCompatActivity activity) {
        super(activity);
    }

    @Provides
    @ActivityScope
    public ViewInteractionActivityPresenter provideViewInteractionActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase firebaseDatabase) {
        return new ViewInteractionActivityPresenter(firebaseAuth,firebaseDatabase);
    }

    @Provides
    @ActivityScope
    public UsersAdapter provideUserEventsAdapter(Context context) {
        return new UsersAdapter(context);
    }

}