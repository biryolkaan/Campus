package release.campus.ui.user_profile;

import dagger.Subcomponent;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 11.3.2017.
 */

@ActivityScope
@Subcomponent(
        modules = {
                UserProfileActivityModule.class
        }
)
public interface UserProfileActivityComponent {
    void injectTo(UserProfileActivity activity);
}

