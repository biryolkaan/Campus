package release.campus.ui.event_details;

import dagger.Subcomponent;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 6.4.2017.
 */

@ActivityScope
@Subcomponent(
        modules = {
                EventDetailsActivityModule.class
        }
)
public interface EventDetailsActivityComponent {
    void injectTo(EventDetailsActivity activity);
}

