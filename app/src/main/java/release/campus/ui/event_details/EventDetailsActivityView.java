package release.campus.ui.event_details;

import java.util.List;

import release.campus.model.Comment;
import release.campus.model.Event;

/**
 * Created by Morgoth on 6.4.2017.
 */

public interface EventDetailsActivityView {
    void onCommentsLoad(List<Comment> commentList);

    void setRefreshing();

    void onCommentSent();

    void onUpdateEvent(Event event);
}
