package release.campus.ui.event_details;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.ActivityModule;
import release.campus.di.scopes.ActivityScope;
import release.campus.ui.base.feed.FeedAdapter;
import release.campus.ui.base.feed.FeedFragmentPresenter;

/**
 * Created by Morgoth on 6.4.2017.
 */

@Module
public class EventDetailsActivityModule extends ActivityModule {

    public EventDetailsActivityModule(AppCompatActivity activity) {
        super(activity);
    }

    @Provides
    @ActivityScope
    public EventDetailsActivityPresenter provideEventDetailsActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        return new EventDetailsActivityPresenter(firebaseAuth,database);
    }

    @Provides
    @ActivityScope
    public FeedFragmentPresenter provideFeedFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        return new FeedFragmentPresenter(firebaseAuth,database);
    }


    @Provides
    @ActivityScope
    public CommentsAdapter provideCommentsAdapter(Context context, EventDetailsActivityPresenter presenter) {
        return new CommentsAdapter(context, presenter);
    }

    @Provides
    @ActivityScope
    public FeedAdapter provideFeedsAdapter(Context context) {
        return new FeedAdapter(context);
    }

}