package release.campus.ui.event_details;

import android.util.Log;
import android.widget.ToggleButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import release.campus.model.Comment;
import release.campus.model.Event;
import release.campus.model.Owner;
import release.campus.model.User;
import release.campus.presenter.Presenter;
import release.campus.ui.base.profile.ProfileFragmentView;
import release.campus.utils.CompositeReference;
import release.campus.utils.FirebaseListenerReference;

/**
 * Created by Morgoth on 6.4.2017.
 */

public class EventDetailsActivityPresenter extends Presenter<EventDetailsActivityView> {

    private DatabaseReference rootRef;
    private List<Comment> commentList;
    private FirebaseUser currentUser;
    private CompositeReference compositeReference;

    public EventDetailsActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        rootRef = database.getReference().child("social");
        this.currentUser = firebaseAuth.getCurrentUser();
    }

    void getComments(String eventId) {
        view.setRefreshing();
        Query commentsRef = rootRef.child("comments").child(eventId).orderByChild("rating");

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                commentList.clear();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Comment comment = snapshot.getValue(Comment.class);
                    if(comment.getVotes()!=null && comment.getVotes().containsKey(currentUser.getUid()) &&
                            comment.getVotes().get(currentUser.getUid()) == 1)
                        comment.setUpvotedByCurrentUser(true);
                    else if(comment.getVotes()!=null && comment.getVotes().containsKey(currentUser.getUid()) &&
                            comment.getVotes().get(currentUser.getUid()) == -1){
                        comment.setDownvotedByCurrentUser(true);
                    }
                    else  {
                        comment.setDownvotedByCurrentUser(false);
                        comment.setUpvotedByCurrentUser(false);
                    }
                    commentList.add(comment);
                }
                if(view!=null){
                    Collections.reverse(commentList);
                    view.onCommentsLoad(commentList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        compositeReference.add(new FirebaseListenerReference(commentsRef, valueEventListener, false));

    }


    boolean isSelected;
    //todo cloud func
    public void vote(final ToggleButton toggleButton, String commentId, String eventId, final boolean upvote) {
        DatabaseReference commentsRef = rootRef.child("comments").child(eventId).child(commentId);
        commentsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Comment c = mutableData.getValue(Comment.class);
                if (c == null) {
                    return Transaction.success(mutableData);
                }

                if (upvote) {
                    if(c.getVotes() == null) {
                        Map<String, Long> votes = new HashMap<>();
                        votes.put(currentUser.getUid(), (long) 1);
                        c.setVotes(votes);
                    }
                    else {
                        if(c.getVotes().containsKey(currentUser.getUid())) {
                            long value = c.getVotes().get(currentUser.getUid());
                            if(value == 1) {
                                c.getVotes().put(currentUser.getUid(), (long) 0);
                            }
                            else
                                c.getVotes().put(currentUser.getUid(), (long) 1);
                        }
                        else
                            c.getVotes().put(currentUser.getUid(), (long) 1);
                    }
                } else {
                    if(c.getVotes() == null) {
                        Map<String, Long> votes = new HashMap<>();
                        votes.put(currentUser.getUid(), (long) -1);
                        c.setVotes(votes);
                    }
                    else {
                        if(c.getVotes().containsKey(currentUser.getUid())) {
                            long value = c.getVotes().get(currentUser.getUid());
                            if(value == -1) {
                                c.getVotes().put(currentUser.getUid(), (long) 0);
                            }
                            else
                                c.getVotes().put(currentUser.getUid(), (long) -1);
                        }
                        else
                            c.getVotes().put(currentUser.getUid(), (long) -1);
                    }
                }

                mutableData.setValue(c);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {

            }
        });

    }

    void sendComment(final String eventId, final String content) {
        DatabaseReference userRef = rootRef.child("users").child(currentUser.getUid());
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User u = dataSnapshot.getValue(User.class);
                String key = rootRef.child("comments").child(eventId).push().getKey();
                Owner owner = new Owner(u.getName(), u.getSurname(), u.getAvatar_url());
                owner.setUid(u.getUid());
                HashMap<String, Object> result = new HashMap<>();
                result.put("content", content);
                result.put("owner", owner);
                result.put("commentId",key);
                result.put("eventId",eventId);
                Map<String, Long> upvoters = new HashMap<>();
                upvoters.put(currentUser.getUid(),(long)1);
                result.put("votes",upvoters);
                rootRef.child("comments").child(eventId).child(key).setValue(result);
                rootRef.child("user_comments").child(currentUser.getUid()).child(key).setValue(eventId);
                if(view!=null)
                    view.onCommentSent();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        compositeReference.add(new FirebaseListenerReference(userRef, valueEventListener, true));
    }

    @Override
    public void onViewAttached(EventDetailsActivityView view) {
        commentList = new ArrayList<>();
        compositeReference = new CompositeReference();
        super.onViewAttached(view);
    }

    @Override
    public void onViewDetached() {
        compositeReference.unsubscribe();
        super.onViewDetached();
    }

    public void getEventDetails(Event myEvent) {
        view.setRefreshing();
        Query eventRef = rootRef.child("events").child(myEvent.getEventId());

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Event event = dataSnapshot.getValue(Event.class);
                if (event!=null && event.upvoters != null && event.upvoters.containsKey(currentUser.getUid()))
                    event.setUpvotedByCurrentUser(true);
                else {
                    if(event!=null)
                        event.setUpvotedByCurrentUser(false);
                }

                if(view!=null && event!=null){
                    view.onUpdateEvent(event);
                }
            };

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        compositeReference.add(new FirebaseListenerReference(eventRef, valueEventListener, true));

    }
}
