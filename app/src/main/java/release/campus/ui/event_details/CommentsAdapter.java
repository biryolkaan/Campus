package release.campus.ui.event_details;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import release.campus.R;
import release.campus.model.Comment;
import release.campus.ui.user_profile.UserProfileActivity;

/**
 * Created by Morgoth on 6.4.2017.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    private Context context;
    private List<Comment> commentList;
    private int[] colors;
    private EventDetailsActivityPresenter presenter;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.comment)
        TextView tv_comment;

        @BindView(R.id.rating)
        TextView rating;

        @BindView(R.id.avatar)
        CircularImageView avatar;

        @BindView(R.id.upvote)
        ToggleButton upvote;

        @BindView(R.id.downvote)
        ToggleButton downvote;


        private Comment comment;
        private EventDetailsActivityPresenter presenter;
        private boolean upvoted = false;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }

        @OnClick(R.id.avatar)
        void setAvatarOnClick() {
            Context context = avatar.getContext();
            Intent i = new Intent(context, UserProfileActivity.class);
            i.putExtra("uid",comment.getOwner().getUid());
            i.putExtra("user", Parcels.wrap(comment.getOwner()));
            context.startActivity(i);
        }

        @OnClick(R.id.upvote)
        void setOnClick() {
            upvoted = true;
            presenter.vote(upvote ,comment.getCommentId(),comment.getEventId(),true);
             // rating.setText(String.valueOf(comment.getRating()+1));
            downvote.setChecked(false);
        }

        @OnClick(R.id.downvote)
        void setOnClick2() {
            presenter.vote(upvote ,comment.getCommentId(),comment.getEventId(),false);
            if(upvoted) {
             //   rating.setText(String.valueOf(comment.getRating()-2));
            }
            else
             //   rating.setText(String.valueOf(comment.getRating()-1));
            upvote.setChecked(false);
            upvoted = false;
        }

        public void bindData(Comment comment, EventDetailsActivityPresenter presenter) {
            this.comment = comment;
            this.presenter = presenter;
        }
    }

    public CommentsAdapter(Context context, EventDetailsActivityPresenter presenter) {
        this.context = context;
        this.presenter = presenter;
        colors = new int[2];
        colors[0] = ContextCompat.getColor(context, R.color.merino);
        colors[1] = ContextCompat.getColor(context,R.color.btnColorHighlight);
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comments, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tv_comment.setText(commentList.get(position).getContent());
        holder.name.setText(commentList.get(position).getOwner().getName() + " " + commentList.get(position).getOwner().getSurname());
        holder.rating.setText(String.valueOf(commentList.get(position).getRating()));
        if(commentList.get(position).isUpvotedByCurrentUser()) {
            holder.upvote.setChecked(true);
        }
        else holder.upvote.setChecked(false);
        if(commentList.get(position).isDownvotedByCurrentUser()) {
            holder.downvote.setChecked(true);
        }
        else holder.downvote.setChecked(false);
        Glide.with(context)
                .load(commentList.get(position).getOwner().getAvatar_url())
                .centerCrop()
                .crossFade()
                .into(holder.avatar);
        holder.bindData(commentList.get(position), presenter);
    }

    public void update(List<Comment> list) {
        this.commentList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (commentList !=null) ? commentList.size() : 0 ;
    }

}

