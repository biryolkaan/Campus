package release.campus.ui.event_details;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.parceler.Parcels;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Comment;
import release.campus.model.Event;
import release.campus.ui.BaseActivity;
import release.campus.ui.base.feed.FeedAdapter;
import release.campus.ui.base.feed.FeedFragmentPresenter;
import release.campus.utils.ValidationUtils;

public class EventDetailsActivity extends BaseActivity implements EventDetailsActivityView, SwipeRefreshLayout.OnRefreshListener {

    @Inject EventDetailsActivityPresenter presenter;
    @Inject FeedFragmentPresenter feed_presenter;
    @Inject CommentsAdapter commentsAdapter;
    @Inject FeedAdapter feedAdapter;

    @BindView(R.id.rv_comments) RecyclerView rv_Comments;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.message) TextView message;
    @BindView(R.id.rating) TextView rating;
    @BindView(R.id.timestamp) TextView timestamp;
    @BindView(R.id.bio) TextView bio;
    @BindView(R.id.tv_date) TextView tv_date;
    @BindView(R.id.tv_location) TextView tv_location;
    @BindView(R.id.avatar) CircularImageView avatar;
    @BindView(R.id.comments) TextView comments;
    @BindView(R.id.swipe_refresh_layout_comments) SwipeRefreshLayout swipeRefreshLayoutComments;
    @BindView(R.id.rv_single_event)RecyclerView rv_Event;
    @BindView(R.id.editText_comment) EditText comment;
    @BindView(R.id.toggle_like) ToggleButton btn_like;
    @BindView(R.id.card_tag_1)
    CardView card_tag_1;
    @BindView(R.id.card_tag_2)
    CardView card_tag_2;
    @BindView(R.id.card_tag_3)
    CardView card_tag_3;
    @BindView(R.id.tv_tag_1)
    TextView tv_tag_1;
    @BindView(R.id.tv_tag_2)
    TextView tv_tag_2;
    @BindView(R.id.tv_tag_3)
    TextView tv_tag_3;

    Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
        setContentView(R.layout.activity_event_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        event = (Event) Parcels.unwrap(getIntent().getParcelableExtra("event"));
        setupSwipeRefreshLayout();
        setupRecyclerView();
        setEvent(event);
        presenter.getEventDetails(event);
        presenter.getComments(event.getEventId());

    }

    void setupSwipeRefreshLayout() {
        swipeRefreshLayoutComments.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
        swipeRefreshLayoutComments.setOnRefreshListener(this);
    }

    private void setupRecyclerView() {
        rv_Comments.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        rv_Comments.setAdapter(commentsAdapter);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        rv_Comments.addItemDecoration(itemDecoration);
        List<Event> eventList = new ArrayList<>();
        eventList.add(event);
        feedAdapter.update(eventList, 0, 0);
        rv_Event.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        rv_Event.setAdapter(feedAdapter);
        message.setMovementMethod(new ScrollingMovementMethod());
    }

    private void setEvent(Event event) {

        title.setText(event.getTitle());
        message.setText(event.getMessage());
        bio.setText(event.getOwner().getBio());
        rating.setText(ValidationUtils.convertWithSuffix(event.getRating()));
        SimpleDateFormat format = new SimpleDateFormat("MM/d/yyyy 'at' h:mm a");
        tv_date.setText(format.format(new Date(event.getEventTime())));
        tv_location.setText(event.getLocationName());
        name.setText(event.getOwner().getName() + " " + event.getOwner().getSurname());
        timestamp.setText(DateUtils.getRelativeTimeSpanString(event.getDateCreated()));
        comments.setText(ValidationUtils.convertWithSuffix(event.getComment_count()));

        if(event.isUpvotedByCurrentUser())
            btn_like.setChecked(true);
        else btn_like.setChecked(false);

        Glide.with(this)
                .load(event.getOwner().getAvatar_url())
                .centerCrop()
                .crossFade()
                .into(avatar);
        getSupportActionBar().setTitle(event.getTitle());


        if(event.getTags().size() == 1) {
            tv_tag_1.setText(event.getTags().get(0));
            card_tag_2.setVisibility(View.GONE);
            tv_tag_2.setVisibility(View.GONE);
            card_tag_3.setVisibility(View.GONE);
            tv_tag_3.setVisibility(View.GONE);

        }
        else if(event.getTags().size() == 2){
            tv_tag_1.setText(event.getTags().get(0));
            tv_tag_2.setText(event.getTags().get(1));
            card_tag_3.setVisibility(View.GONE);
            tv_tag_3.setVisibility(View.GONE);

        }
        else {
            tv_tag_1.setText(event.getTags().get(0));
            tv_tag_2.setText(event.getTags().get(1));
            tv_tag_3.setText(event.getTags().get(2));

        }
    }

    @OnClick(R.id.toggle_like)
    void setLikeOnClick() {
        feed_presenter.vote(event.getEventId());
        if (btn_like.isChecked()) {
           rating.setText( String.valueOf(Long.parseLong(rating.getText().toString()) + 1));
        }
        else
            rating.setText( String.valueOf(Long.parseLong(rating.getText().toString()) - 1));
    }

    @OnClick(R.id.iv_location)
    void setLocationOnClick() {
        Uri gmmIntentUri = Uri.parse("geo:" + event.getLatitude() + "," + event.getLongitude());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
        else {
            Toast.makeText(this, "You don't have Google Maps installed.", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_sendComment)
    void setSendCommentClick() {
        if(ValidationUtils.isEmpty(comment.getText().toString())) {
            presenter.sendComment(event.getEventId(), comment.getText().toString());
        }
        else {
            Toast.makeText(this, getString(R.string.error_empty_comment), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCommentSent() {
        comment.getText().clear();
        comment.clearFocus();
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(comment.getWindowToken(), 0);
        presenter.getComments(event.getEventId());
    }

    @Override
    protected void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new EventDetailsActivityModule(this)).injectTo(this);
    }

    @Override
    public void onCommentsLoad(List<Comment> commentList) {
        commentsAdapter.update(commentList);
        swipeRefreshLayoutComments.setRefreshing(false);

    }

    @Override
    public void setRefreshing() {
        swipeRefreshLayoutComments.setRefreshing(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        presenter.getComments(event.getEventId());
    }

    @Override
    public void onUpdateEvent(Event event) {
        rating.setText(ValidationUtils.convertWithSuffix(event.getRating()));
        comments.setText(ValidationUtils.convertWithSuffix(event.getComment_count()));
        if(event.isUpvotedByCurrentUser())
            btn_like.setChecked(true);
        else btn_like.setChecked(false);
    }
}
