package release.campus.ui.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.ButterKnife;
import butterknife.OnClick;
import release.campus.R;
import release.campus.ui.base.BaseFragmentActivity;
import release.campus.ui.login.LoginActivity;
import release.campus.ui.signup.SignUpActivity;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //TODo http://stackoverflow.com/questions/35960546/firebase-still-retrieving-authdata-after-deletion
        if(currentUser!=null && currentUser.isEmailVerified()) {
            Toast.makeText(this, currentUser.getEmail().toString(), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, BaseFragmentActivity.class));
            finish();
        }
   /*     videoView = (VideoView) findViewById(R.id.videoView);

        Uri video = Uri.parse("https://firebasestorage.googleapis.com/v0/b/campus-51915.appspot.com/o/a3.mp4?alt=media&token=4aabfbc7-d871-45dd-8e0e-287ab1ede24b");
        videoView.setVideoURI(video);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                videoView.start();
            }
        });*/

    }

    @OnClick(R.id.btn_signin)
    void setSignup() {

        startActivity(new Intent(this, LoginActivity.class));
    }

    @OnClick(R.id.btn_signup)
    void setSignin() {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
