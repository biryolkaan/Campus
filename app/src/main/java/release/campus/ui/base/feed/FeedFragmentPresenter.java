package release.campus.ui.base.feed;

import android.support.v4.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import release.campus.model.Event;
import release.campus.presenter.Presenter;
import release.campus.utils.CompositeReference;
import release.campus.utils.Constants;
import release.campus.utils.FirebaseListenerReference;

/**
 * Created by Morgoth on 4.3.2017.
 */

public class FeedFragmentPresenter extends Presenter<FeedFragmentView> {

    private static final String PAGE_END_OFFSET = "0";
    private static final int PAGE_LIMIT = 10;

    private String mNextChildKey = "";
    private CompositeReference compositeReference;
    private DatabaseReference rootRef;
    private List<Event> eventList;
    private FirebaseUser currentUser;
    private int previousSize = 0;
    private List<String> snapshotList;
    private List<String> tmpSnapshotList;
    private long count = 0;

    public FeedFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database, Fragment fragment) {
        this.rootRef = FirebaseDatabase.getInstance().getReference().child("social");
        currentUser = firebaseAuth.getCurrentUser();
    }

    public FeedFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        this.rootRef = FirebaseDatabase.getInstance().getReference().child("social");
        currentUser = firebaseAuth.getCurrentUser();
    }

    void fillFeed(boolean reset) {
        view.setRefreshing();
        Query timelineRef;
        if(reset) {
            snapshotList.clear();
            tmpSnapshotList.clear();
            mNextChildKey = "";
            previousSize = 0;
            eventList.clear();
        }

        final List<Event> tmpEventList = new ArrayList<>();
        if(mNextChildKey.isEmpty()) {
            timelineRef  = rootRef.child("timeline").child(currentUser.getUid()).orderByKey()
                    .limitToLast(Constants.PAGE_LIMIT);
        }
        else {
            timelineRef = rootRef.child("timeline").child(currentUser.getUid()).orderByKey()
                    .limitToLast(Constants.PAGE_LIMIT).endAt(mNextChildKey);
        }

        ValueEventListener valuEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(view!=null && !dataSnapshot.exists()) {
                    view.updateEvents(eventList, previousSize, eventList.size());
                }

                count = dataSnapshot.getChildrenCount();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Query query = rootRef.child("events").child(snapshot.getKey());

                    if(!snapshotList.contains(snapshot.getKey())) {
                        snapshotList.add(snapshot.getKey());
                        tmpSnapshotList.add(snapshot.getKey());
                    }

                    ValueEventListener valueEventListener1 = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            Event event = dataSnapshot.getValue(Event.class);
                            if (event!=null && event.upvoters != null && event.upvoters.containsKey(currentUser.getUid()))
                                event.setUpvotedByCurrentUser(true);
                            else {
                                if(event!=null)
                                    event.setUpvotedByCurrentUser(false);
                            }
                            tmpEventList.add(event);

                            if (view != null &&  count >= Constants.PAGE_LIMIT && tmpEventList.size()>=Constants.PAGE_LIMIT) {
                                if(tmpSnapshotList.size()>0)
                                    mNextChildKey = tmpSnapshotList.get(0);
                                tmpSnapshotList.clear();
                                Collections.reverse(tmpEventList);
                                previousSize = eventList.size();
                                eventList.addAll(tmpEventList);
                                tmpEventList.clear();
                                view.updateEvents(eventList, previousSize, eventList.size());
                            }
                            else if(view!=null && count < Constants.PAGE_LIMIT && tmpEventList.size() >= count) {
                                if(tmpSnapshotList.size()>0)
                                    mNextChildKey = tmpSnapshotList.get(0);
                                tmpSnapshotList.clear();
                                Collections.reverse(tmpEventList);
                                previousSize = eventList.size();
                                eventList.addAll(tmpEventList);
                                tmpEventList.clear();
                                view.updateEvents(eventList, previousSize, eventList.size());
                            }
                            else if(view!=null) {
                                view.stopRefreshing();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    };

                    compositeReference.add(new FirebaseListenerReference(query,valueEventListener1,true));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        compositeReference.add(new FirebaseListenerReference(timelineRef,valuEventListener,true));
    }


    private Event updatedEvent;
    public void vote(String eventId) {
        final DatabaseReference eventRef = rootRef.child("events").child(eventId);
        eventRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Event e = mutableData.getValue(Event.class);
                if (e == null) {
                    return Transaction.success(mutableData);
                }
                if(e.upvoters!=null) {
                    if(!e.upvoters.containsKey(currentUser.getUid())) {
                        e.upvoters.put(currentUser.getUid(),true);
                        e. rating = e.rating + 1;
                    }
                    else {
                        e.upvoters.put(currentUser.getUid(), null);
                        e.rating = e.rating - 1;
                    }
                }
                else {
                    Map<String,Boolean> upvoters = new HashMap<>();
                    upvoters.put(currentUser.getUid(),true);
                    e.rating = e.rating + 1;
                    e.upvoters = upvoters;

                }
                updatedEvent = e;
                mutableData.setValue(e);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
            }
        });
    }

    @Override
    public void onViewAttached(FeedFragmentView view) {
        super.onViewAttached(view);
        eventList = new ArrayList<>();
        snapshotList = new ArrayList<>();
        tmpSnapshotList = new ArrayList<>();
        compositeReference = new CompositeReference();
    }

    @Override
    public void onViewDetached() {
        compositeReference.unsubscribe();
        super.onViewDetached();
    }
}
