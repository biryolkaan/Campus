package release.campus.ui.base.explore;

import dagger.Subcomponent;
import release.campus.di.scopes.FragmentScope;

/**
 * Created by Morgoth on 14.3.2017.
 */
@FragmentScope
@Subcomponent(
        modules = {
                ExploreFragmentModule.class
        }
)
public interface ExploreFragmentComponent {
    void injectTo(ExploreFragment fragment);
}
