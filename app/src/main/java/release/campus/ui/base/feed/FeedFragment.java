package release.campus.ui.base.feed;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Event;
import release.campus.ui.BaseFragment;
import release.campus.ui.send_event.SendEventActivity;
import release.campus.utils.EndlessRecyclerViewScrollListener;
import release.campus.utils.FeedItemAnimator;
import release.campus.utils.FirebaseListenerReference;

public class FeedFragment extends BaseFragment implements FeedFragmentView, SwipeRefreshLayout.OnRefreshListener {

    @Inject FeedFragmentPresenter presenter;
    @Inject FirebaseAuth mAuth;
    @Inject LinearLayoutManager linearLayoutManager;
    @Inject FeedAdapter adapter;

    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.rv_feed) RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.empty_view)
    TextView emptyView;

    private long itemCount = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.e("FeedFragment", "onViewCreated");
        super.onViewCreated(view,savedInstanceState);
        fab.show();
        setupSwipeRefreshLayout();
        setupRecyclerView();
        presenter.fillFeed(true);


        DatabaseReference countRef  = FirebaseDatabase.getInstance().getReference().child("social")
                .child("timeline").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                itemCount = dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        countRef.addListenerForSingleValueEvent(valueEventListener);

    }

    @Override
    public void onDestroyView() {
        Log.e("FeedFragment", "OnDestroyView");
        presenter.onViewDetached();
        super.onDestroyView();
    }

    void setupRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new FeedItemAnimator());
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager,fab) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if(totalItemsCount!=-1 && totalItemsCount<itemCount)
                    presenter.fillFeed(false);
            }
        });

    }

    void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new FeedFragmentModule(this)).injectTo(this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_feed;
    }

    @Override
    public void updateEvents(List<Event> eventList, int previousSize, int size) {
        swipeRefreshLayout.setRefreshing(false);
        if(eventList.size()==0) {
            emptyView.setVisibility(View.VISIBLE);
        }
        else {
            emptyView.setVisibility(View.GONE);
            adapter.update(eventList, previousSize, size);
        }
    }

    @Override
    public void setRefreshing() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void stopRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @OnClick(R.id.fab)
    void setFabOnClick() {
        startActivity(new Intent(getActivity(), SendEventActivity.class));
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_fade_out_left);
    }

    @Override
    public void onRefresh() {
        adapter.notifyRemoved();
        presenter.fillFeed(true);
    }

}
