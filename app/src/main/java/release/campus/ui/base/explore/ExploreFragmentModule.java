package release.campus.ui.base.explore;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.FragmentModule;
import release.campus.di.scopes.FragmentScope;
import release.campus.ui.base.feed.FeedAdapter;
import release.campus.ui.base.feed.FeedFragmentPresenter;

/**
 * Created by Morgoth on 14.3.2017.
 */
@Module
public class ExploreFragmentModule extends FragmentModule{

    public ExploreFragmentModule(Fragment fragment) {
        super(fragment);
    }

    @Provides
    @FragmentScope
    public ExploreFragmentPresenter provideExploreFragmentPresenter(FirebaseDatabase database, FirebaseAuth mAuth) {
        return new ExploreFragmentPresenter(database,mAuth);
    }

    //TODO FIX
    @Provides
    @FragmentScope
    public FeedFragmentPresenter provideFeedFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database, Fragment fragment) {
        return new FeedFragmentPresenter(firebaseAuth,database,fragment);
    }

    @Provides
    @FragmentScope
    public FeedAdapter provideFeedAdapter(Context context, FeedFragmentPresenter presenter) {
        return new FeedAdapter(context, presenter);
    }

    @Provides
    @FragmentScope
    public LinearLayoutManager provideLinearLayoutManager(Context context) {
        return new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
    }
}
