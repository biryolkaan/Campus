package release.campus.ui.base.profile;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mikhaellopez.circularimageview.CircularImageView;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import release.campus.R;
import release.campus.model.Event;
import release.campus.ui.event_details.EventDetailsActivity;
import release.campus.ui.user_profile.UserProfileActivityPresenter;
import release.campus.utils.ValidationUtils;

/**
 * Created by Morgoth on 5.3.2017.
 */

public class UserEventsAdapter extends RecyclerView.Adapter<UserEventsAdapter.ViewHolder> {
    private Context context;
    private List<Event> eventList;
    private UserProfileActivityPresenter userPresenter;
    private ProfileFragmentPresenter profiletPresenter;
    private int[] colors;
    private int size;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card)
        CardView card;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.message)
        TextView message;
        @BindView(R.id.rating)
        TextView rating;
        @BindView(R.id.comments)
        TextView comments;
        @BindView(R.id.timestamp)
        TextView timestamp;
        @BindView(R.id.toggle_like)
        ToggleButton upvote;
        @BindView(R.id.iv_comments)
        ImageView iv_comments;

        private Event event;
        private UserProfileActivityPresenter userPresenter;
        private ProfileFragmentPresenter profilePresenter;
        private boolean upvoted = false;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }

        public void bindData(Event event, UserProfileActivityPresenter presenter) {
            this.event = event;
            this.userPresenter = presenter;
        }

        public void bindData(Event event, ProfileFragmentPresenter presenter) {
            this.event = event;
            this.profilePresenter = presenter;
        }

        @OnClick(R.id.card)
        void setCardOnClick() {
            Context context = title.getContext();
            Intent i = new Intent(context, EventDetailsActivity.class);
            i.putExtra("event", Parcels.wrap(event));
            //todo fix or remove
            Pair<View, String> p3 = Pair.create((View)title, "transition_title");
            Pair<View, String> p5 = Pair.create((View)timestamp, "transition_timestamp");
            Pair<View, String> p6 = Pair.create((View)card, "transition_card");
            Pair<View, String> p7 = Pair.create((View)upvote, "transition_like");
            Pair<View, String> p8 = Pair.create((View)rating, "transition_rating");
            Pair<View, String> p9 = Pair.create((View)iv_comments, "transition__iv_comments");
            Pair<View, String> p10 = Pair.create((View)comments, "transition_comments");
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation((Activity) context, p3, p5, p6, p7, p8, p9, p10);
            context.startActivity(i);
            ((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_fade_out_left);
        }

        @OnClick(R.id.toggle_like)
        void setUpVoteOnClick() {
            if(!upvote.isChecked()) {
                int val = Integer.valueOf(rating.getText().toString()) - 1;
                rating.setText(String.valueOf(val));
            }
            else if(upvote.isChecked()) {
                int val = Integer.valueOf(rating.getText().toString()) + 1;
                rating.setText(String.valueOf(val));
            }
            if(profilePresenter!=null)
                profilePresenter.vote(event.getEventId());
            else if(userPresenter!=null)
                userPresenter.vote(event.getEventId());
        }

    }

    public UserEventsAdapter(Context context, UserProfileActivityPresenter userProfileActivityPresenter) {
        this.context = context;
        this.userPresenter = userProfileActivityPresenter;
        colors = new int[2];
        colors[0] = ContextCompat.getColor(context, R.color.merino);
        colors[1] = ContextCompat.getColor(context,R.color.btnColorHighlight);
    }

    public UserEventsAdapter(Context context, ProfileFragmentPresenter profileFragmentPresenter) {
        this.context = context;
        this.profiletPresenter = profileFragmentPresenter;
        colors = new int[2];
        colors[0] = ContextCompat.getColor(context, R.color.merino);
        colors[1] = ContextCompat.getColor(context,R.color.btnColorHighlight);
    }



    public UserEventsAdapter(Context context) {
        this.context = context;
        colors = new int[2];
        colors[0] = ContextCompat.getColor(context, R.color.merino);
        colors[1] = ContextCompat.getColor(context,R.color.btnColorHighlight);
    }

    @Override
    public UserEventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_events, parent, false);
        return new UserEventsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final UserEventsAdapter.ViewHolder holder, final int position) {

        holder.title.setText(eventList.get(position).getTitle());
        holder.message.setText(eventList.get(position).getMessage());
        holder.rating.setText(ValidationUtils.convertWithSuffix(eventList.get(position).getRating()));
        holder.timestamp.setText(DateUtils.getRelativeTimeSpanString(eventList.get(position).getDateCreated()));
        if(eventList.get(position).isUpvotedByCurrentUser()) {
            holder.upvote.setChecked(true);
        }
        else holder.upvote.setChecked(false);

        holder.comments.setText(ValidationUtils.convertWithSuffix(eventList.get(position).getComment_count()));
        if(userPresenter!=null)
            holder.bindData(eventList.get(position), userPresenter);
        else if(profiletPresenter!=null)
            holder.bindData(eventList.get(position), profiletPresenter);
    }

    public void update(List<Event> list, int previousSize, int size) {
        this.eventList = list;
        this.size = size;
        notifyItemRangeInserted(previousSize, size);
    }

    public void notifyRemoved() {
        notifyItemRangeRemoved(0, size);
    }


    @Override
    public int getItemCount() {
        return (eventList !=null) ? eventList.size() : 0 ;
    }

}

