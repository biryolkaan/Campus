package release.campus.ui.base.chat;

import java.util.List;

import release.campus.model.Chat;

/**
 * Created by Morgoth on 15.4.2017.
 */

public interface ChatFragmentView {
    void onChatsLoad(List<Chat> chatList);

    void setRefreshing();
}
