package release.campus.ui.base.profile;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.FragmentModule;
import release.campus.di.scopes.FragmentScope;

/**
 * Created by Morgoth on 2.3.2017.
 */

@Module
public class ProfileFragmentModule extends FragmentModule {

    public ProfileFragmentModule(Fragment fragment) {
        super(fragment);
    }

    @Provides
    @FragmentScope
    public ProfileFragmentPresenter provideSignUpFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        return new ProfileFragmentPresenter(firebaseAuth,database);
    }

    @Provides
    @FragmentScope
    public UserEventsAdapter provideUserEventsAdapter(Context context, ProfileFragmentPresenter presenter) {
        return new UserEventsAdapter(context, presenter);
    }

    @Provides
    @FragmentScope
    public LinearLayoutManager provideLinearLayoutManager(Context context) {
        return new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
    }


}