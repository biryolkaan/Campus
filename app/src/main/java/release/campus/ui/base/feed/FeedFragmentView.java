package release.campus.ui.base.feed;

import java.util.List;

import release.campus.model.Event;

/**
 * Created by Morgoth on 4.3.2017.
 */

public interface FeedFragmentView {
    void updateEvents(List<Event> eventList, int previousSize, int size);
    void setRefreshing();
    void stopRefreshing();
}
