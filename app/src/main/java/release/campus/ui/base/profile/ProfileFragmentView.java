package release.campus.ui.base.profile;

import java.util.List;

import release.campus.model.Event;
import release.campus.model.User;

/**
 * Created by Morgoth on 2.3.2017.
 */

public interface ProfileFragmentView {
    void setInfo(User u);
    void updateEvents(List<Event> eventList, int previousSize, int size);

    void setRefreshing();
}
