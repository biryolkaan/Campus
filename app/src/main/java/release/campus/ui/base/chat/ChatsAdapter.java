package release.campus.ui.base.chat;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import release.campus.R;
import release.campus.model.Chat;
import release.campus.model.Owner;
import release.campus.ui.chat.ChatActivity;

/**
 * Created by Morgoth on 15.4.2017.
 */


public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {

    private Context context;
    private List<Chat> chatList;
    private ChatFragmentPresenter presenter;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_username)
        TextView tv_username;
        @BindView(R.id.tv_latestMessage)
        TextView tv_latestMessage;
        @BindView(R.id.profile_picture)
        CircularImageView avatar;

        private Chat chat;
        private ChatFragmentPresenter presenter;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }

        public void bindData(Chat chat, ChatFragmentPresenter presenter) {
            this.chat = chat;
            this.presenter = presenter;
        }

        @OnClick(R.id.root_view)
        void setOnClick() {
            Context context = tv_username.getContext();
            Intent i = new Intent(context, ChatActivity.class);
            Owner o = new Owner();
            o.setUid(chat.getChatReceiverId());
            o.setName(chat.getChatReceiverName());
            o.setSurname("");
            o.setAvatar_url(chat.getChatReceiverAvatar());
            i.putExtra("receiver", Parcels.wrap(o));
            i.putExtra("chat",Parcels.wrap(chat));
            context.startActivity(i);
            ((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_fade_out_left);
        }

        @OnLongClick(R.id.root_view)
        boolean setOnLongClick() {
            Context context = tv_username.getContext();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getString(R.string.chat_long_click_title));
            builder.setMessage(context.getString(R.string.chat_long_click_message));

            String positiveText = context.getString(android.R.string.ok);
            builder.setPositiveButton(positiveText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            presenter.removeChat(chat);
                        }
                    });

            String negativeText = context.getString(android.R.string.cancel);
            builder.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // negative button logic
                        }
                    });

            AlertDialog dialog = builder.create();
            // display dialog
            dialog.show();
            return true;

        }

    }

    public ChatsAdapter(Context context, ChatFragmentPresenter presenter) {
        this.context = context;
        this.presenter = presenter;
    }

    @Override
    public ChatsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_title, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tv_username.setText(chatList.get(position).getChatReceiverName());
        holder.tv_latestMessage.setText(chatList.get(position).getLatestMessage());
        Glide.with(context)
                .load(chatList.get(position).getChatReceiverAvatar())
                .centerCrop()
                .crossFade()
                .into(holder.avatar);
        holder.bindData(chatList.get(position),presenter);
    }

    public void update(List<Chat> list) {
        this.chatList = list;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return (chatList !=null) ? chatList.size() : 0 ;
    }

}


