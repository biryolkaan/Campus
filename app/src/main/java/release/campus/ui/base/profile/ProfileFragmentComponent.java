package release.campus.ui.base.profile;

import dagger.Subcomponent;
import release.campus.di.scopes.FragmentScope;

/**
 * Created by Morgoth on 2.3.2017.
 */

@FragmentScope
@Subcomponent(
        modules = {
                ProfileFragmentModule.class
        }
)
public interface ProfileFragmentComponent {
    void injectTo(ProfileFragment fragment);
}

