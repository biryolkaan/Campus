package release.campus.ui.base.explore;

import android.provider.ContactsContract;
import android.text.TextUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import release.campus.model.Event;
import release.campus.presenter.Presenter;
import release.campus.utils.CompositeReference;
import release.campus.utils.Constants;
import release.campus.utils.FirebaseListenerReference;

/**
 * Created by Morgoth on 14.3.2017.
 */

public class ExploreFragmentPresenter extends Presenter<ExploreFragmentView> {

    private String mNextChildKey = "";
    private CompositeReference compositeReference;
    private DatabaseReference rootRef;
    private List<Event> eventList;
    private FirebaseUser currentUser;
    private int previousSize = 0;
    private List<String> snapshotList;
    private List<String> tmpSnapshotList;

    public ExploreFragmentPresenter(FirebaseDatabase database, FirebaseAuth mAuth) {
        this.rootRef = database.getReference().child("social");
        this.currentUser = mAuth.getCurrentUser();
    }

    void fillFeed(boolean reset) {
        view.setRefreshing();
        if(reset) {
            snapshotList.clear();
            tmpSnapshotList.clear();
            mNextChildKey = "";
            previousSize = 0;
            eventList.clear();
        }
        final List<Event> tmpEventList = new ArrayList<>();
        Query eventsRef;
        if(mNextChildKey.isEmpty()) {
            eventsRef  = rootRef.child("events").orderByKey().limitToLast(Constants.PAGE_LIMIT);
        }
        else {
            eventsRef = rootRef.child("events").orderByKey().limitToLast(Constants.PAGE_LIMIT).endAt(mNextChildKey);
        }

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    if(!snapshotList.contains(snapshot.getKey())) {
                        Event event = snapshot.getValue(Event.class);
                        if (event!=null && event.upvoters != null && event.upvoters.containsKey(currentUser.getUid()))
                            event.setUpvotedByCurrentUser(true);
                        else event.setUpvotedByCurrentUser(false);
                        tmpEventList.add(event);
                        snapshotList.add(snapshot.getKey());
                        tmpSnapshotList.add(snapshot.getKey());
                    }
                }
                if(view!=null) {
                    if(tmpSnapshotList.size()>0)
                        mNextChildKey = tmpSnapshotList.get(0);
                    tmpSnapshotList.clear();
                    Collections.reverse(tmpEventList);
                    previousSize = eventList.size();
                    eventList.addAll(tmpEventList);
                    tmpEventList.clear();
                    view.updateEvents(eventList, previousSize, eventList.size());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


    /*    ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                eventList.clear();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Event event = snapshot.getValue(Event.class);
                    if (event!=null && event.upvoters != null && event.upvoters.containsKey(currentUser.getUid()))
                        event.setUpvotedByCurrentUser(true);
                    else event.setUpvotedByCurrentUser(false);
                    eventList.add(event);
                    mNextChildKey = snapshot.getKey();
                }
                if(view!=null) {
                    Collections.reverse(eventList);
                    view.updateEvents(eventList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };*/
        compositeReference.add(new FirebaseListenerReference(eventsRef,valueEventListener,true));
    }

    @Override
    public void onViewAttached(ExploreFragmentView view) {
        super.onViewAttached(view);
        compositeReference = new CompositeReference();
        eventList = new ArrayList<>();
        snapshotList = new ArrayList<>();
        tmpSnapshotList = new ArrayList<>();
    }

    @Override
    public void onViewDetached() {
        compositeReference.unsubscribe();
        super.onViewDetached();
    }
}
