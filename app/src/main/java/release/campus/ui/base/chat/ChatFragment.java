package release.campus.ui.base.chat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Chat;
import release.campus.ui.BaseFragment;

/**
 * Created by Morgoth on 15.4.2017.
 */

public class ChatFragment extends BaseFragment implements ChatFragmentView, SwipeRefreshLayout.OnRefreshListener {

    @Inject ChatFragmentPresenter presenter;
    @Inject
    ChatsAdapter adapter;
    @Inject LinearLayoutManager linearLayoutManager;
    @Inject RecyclerView.ItemDecoration itemDecoration;
    @BindView(R.id.rv_chat_titles)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.empty_view)
    TextView emptyView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        setupRecyclerView();
        setupSwipeRefreshLayout();
        presenter.getUserChats();
    }

    void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        presenter.onViewDetached();
        super.onDestroyView();
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new ChatFragmentModule(this)).injectTo(this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_chat;
    }

    @Override
    public void onChatsLoad(List<Chat> chatList) {
        if(chatList.size()==0) {
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            adapter.update(chatList);
        }
        else {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter.update(chatList);
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setRefreshing() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onRefresh() {
        presenter.getUserChats();
    }
}
