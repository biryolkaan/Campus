package release.campus.ui.base.feed;

import dagger.Subcomponent;
import release.campus.di.scopes.FragmentScope;

/**
 * Created by Morgoth on 4.3.2017.
 */

@FragmentScope
@Subcomponent(
        modules = {
                FeedFragmentModule.class
        }
)
public interface FeedFragmentComponent {
    void injectTo(FeedFragment fragment);
}

