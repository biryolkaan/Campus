package release.campus.ui.base.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.mikhaellopez.circularimageview.CircularImageView;


import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Event;
import release.campus.model.User;
import release.campus.ui.BaseFragment;
import release.campus.ui.edit_profile.EditProfileActivity;
import release.campus.ui.user_profile.view_interaction.ViewInteractionActivity;
import release.campus.utils.EndlessRecyclerViewScrollListener;
import release.campus.utils.FeedItemAnimator;
import release.campus.utils.ValidationUtils;

/**
 * Created by Morgoth on 2.3.2017.
 */

public class ProfileFragment extends BaseFragment implements ProfileFragmentView, SwipeRefreshLayout.OnRefreshListener   {

    @Inject ProfileFragmentPresenter presenter;
    @Inject UserEventsAdapter adapter;
    @Inject LinearLayoutManager linearLayoutManager;
    @Inject FirebaseAuth mAuth;

    @BindView(R.id.name) TextView name;
    @BindView(R.id.bio) TextView bio;
    @BindView(R.id.followers) TextView followers;
    @BindView(R.id.following) TextView following;
    @BindView(R.id.tv_followers) TextView tv_followers;
    @BindView(R.id.tv_following) TextView tv_following;
    @BindView(R.id.rv_user_events) RecyclerView recyclerView;
    @BindView(R.id.profile_picture) CircularImageView profile_picture;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.progress_overlay) FrameLayout progress_bar;
    @BindView(R.id.empty_view) TextView emptyView;

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        setupRecyclerView();
        setupSwipeRefreshLayout();
        presenter.getProfileInfo();
        presenter.getUserEvents(false);
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new FeedItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager,fab) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                 presenter.getUserEvents(false);
            }
        });
    }

    @Override
    public void onDestroyView() {
        presenter.onViewDetached();
        super.onDestroyView();
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new ProfileFragmentModule(this)).injectTo(this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_profile;
    }

    @Override
    public void setInfo(User u) {
        name.setText(u.name + " " + u.surname);
        bio.setText(u.getBio());
        followers.setText(ValidationUtils.convertWithSuffix(u.getFollower_count()));
        following.setText(ValidationUtils.convertWithSuffix((u.getFollowing_count())));
        Glide.with(this)
                .load(u.getAvatar_url())
                .centerCrop()
                .crossFade()
                .into(profile_picture);

        fab.show();
        tv_followers.setVisibility(View.VISIBLE);
        tv_following.setVisibility(View.VISIBLE);
        progress_bar.setVisibility(View.GONE);
    }

    @OnClick(R.id.fab)
    void setOnClickFab() {
        startActivity(new Intent(this.getActivity(), EditProfileActivity.class));
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_fade_out_left);
    }

    @OnClick(R.id.followers)
    void setFollowersOnClick() {
        Intent i = new Intent(this.getActivity(), ViewInteractionActivity.class);
        i.putExtra("interaction",true);
        startActivity(i);
    }

    @OnClick(R.id.following)
    void setFollowingOnClick() {
        Intent i = new Intent(this.getActivity(), ViewInteractionActivity.class);
        i.putExtra("interaction",false);
        startActivity(i);
    }

    void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void updateEvents(List<Event> eventList, int previousSize, int size) {
        swipeRefreshLayout.setRefreshing(false);
        if(eventList.size()==0) {
            emptyView.setVisibility(View.VISIBLE);
        }
        else {
            emptyView.setVisibility(View.GONE);
            adapter.update(eventList, previousSize, size);
        }
    }

    @Override
    public void setRefreshing() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onRefresh() {
        adapter.notifyRemoved();
        presenter.getProfileInfo();
        presenter.getUserEvents(true);
    }
}