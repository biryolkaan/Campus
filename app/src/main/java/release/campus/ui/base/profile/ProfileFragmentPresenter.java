package release.campus.ui.base.profile;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import release.campus.model.Event;
import release.campus.model.User;
import release.campus.presenter.Presenter;
import release.campus.utils.CompositeReference;
import release.campus.utils.Constants;
import release.campus.utils.FirebaseListenerReference;

/**
 * Created by Morgoth on 2.3.2017.
 */

public class ProfileFragmentPresenter extends Presenter<ProfileFragmentView> {


    private CompositeReference compositeReference;
    private FirebaseUser currentUser;
    private DatabaseReference rootRef;
    private List<Event> eventList;
    private int previousSize = 0;
    private List<String> snapshotList;
    private List<String> tmpSnapshotList;
    private String mNextChildKey = "";

    public ProfileFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        this.currentUser = firebaseAuth.getCurrentUser();

        rootRef = database.getReference().child("social");
    }

    void getProfileInfo() {
        view.setRefreshing();
        DatabaseReference ref = rootRef.child("users").child(currentUser.getUid());
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User u = dataSnapshot.getValue(User.class);
                if(view!=null && u!=null)
                    view.setInfo(u);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.w("F", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        compositeReference.add(new FirebaseListenerReference(ref,postListener,false));
    }


    public void vote(String eventId) {
        final DatabaseReference eventRef = rootRef.child("events").child(eventId);
        eventRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Event e = mutableData.getValue(Event.class);
                if (e == null) {
                    return Transaction.success(mutableData);
                }
                if(e.upvoters!=null) {
                    if(!e.upvoters.containsKey(currentUser.getUid())) {
                        e.upvoters.put(currentUser.getUid(),true);
                        e. rating = e.rating + 1;
                    }
                    else {
                        e.upvoters.put(currentUser.getUid(), null);
                        e.rating = e.rating - 1;
                    }
                }
                else {
                    Map<String,Boolean> upvoters = new HashMap<>();
                    upvoters.put(currentUser.getUid(),true);
                    e.rating = e.rating + 1;
                    e.upvoters = upvoters;

                }
                mutableData.setValue(e);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {

            }
        });
    }


    void getUserEvents(boolean reset) {
        if(reset) {
            snapshotList.clear();
            tmpSnapshotList.clear();
            mNextChildKey = "";
            previousSize = 0;
            eventList.clear();
        }

        final List<Event> tmpEventList = new ArrayList<>();

        Query userRef;
        if(mNextChildKey.isEmpty()) {
            userRef = rootRef.child("user_events").child(currentUser.getUid()).orderByKey().limitToLast(Constants.PAGE_LIMIT);
        }
        else {
            userRef = rootRef.child("user_events").child(currentUser.getUid()).orderByKey()
                    .limitToLast(Constants.PAGE_LIMIT).endAt(mNextChildKey);
        }

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()) {
                    if(view!=null)
                        view.updateEvents(eventList, previousSize, eventList.size());
                }
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    if(!snapshotList.contains(snapshot.getKey())) {
                        Event event = snapshot.getValue(Event.class);
                        if (event!=null && event.upvoters != null && event.upvoters.containsKey(currentUser.getUid()))
                            event.setUpvotedByCurrentUser(true);
                        else event.setUpvotedByCurrentUser(false);
                        tmpEventList.add(event);
                        snapshotList.add(snapshot.getKey());
                        tmpSnapshotList.add(snapshot.getKey());
                    }
                }
                if(view!=null) {
                    if(tmpSnapshotList.size()>0)
                        mNextChildKey = tmpSnapshotList.get(0);
                    tmpSnapshotList.clear();
                    Collections.reverse(tmpEventList);
                    previousSize = eventList.size();
                    eventList.addAll(tmpEventList);
                    tmpEventList.clear();
                    view.updateEvents(eventList, previousSize, eventList.size());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        compositeReference.add(new FirebaseListenerReference(userRef,valueEventListener,true));
    }

    @Override
    public void onViewAttached(ProfileFragmentView view) {
        super.onViewAttached(view);
        compositeReference = new CompositeReference();
        eventList = new ArrayList<>();
        snapshotList = new ArrayList<>();
        tmpSnapshotList = new ArrayList<>();
    }

    @Override
    public void onViewDetached() {
        compositeReference.unsubscribe();
        super.onViewDetached();
    }


}
