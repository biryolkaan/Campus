package release.campus.ui.base.chat;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.FragmentModule;
import release.campus.di.scopes.FragmentScope;

/**
 * Created by Morgoth on 15.4.2017.
 */

@Module
public class ChatFragmentModule extends FragmentModule {

    public ChatFragmentModule(Fragment fragment) {
        super(fragment);
    }

    @Provides
    @FragmentScope
    public ChatFragmentPresenter provideChatFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database, Fragment fragment) {
        return new ChatFragmentPresenter(firebaseAuth,database,fragment);
    }

    @Provides
    @FragmentScope
    public ChatsAdapter provideChatAdapter(Context context,ChatFragmentPresenter presenter) {
        return new ChatsAdapter(context, presenter);
    }

    @Provides
    @FragmentScope
    public LinearLayoutManager provideLinearLayoutManager(Context context) {
        return new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
    }

    @Provides
    @FragmentScope
    public RecyclerView.ItemDecoration provideItemDecoration(Context context) {
        return new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
    }

}