package release.campus.ui.base.feed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import release.campus.R;
import release.campus.model.Event;
import release.campus.ui.event_details.EventDetailsActivity;
import release.campus.ui.user_profile.UserProfileActivity;
import release.campus.ui.user_profile.view_interaction.ViewInteractionActivity;
import release.campus.utils.ValidationUtils;

/**
 * Created by Morgoth on 4.3.2017.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {

    private Context context;
    private List<Event> eventList;
    private FeedFragmentPresenter presenter;

    private int size;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card)
        CardView card;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.message)
        TextView message;
        @BindView(R.id.rating)
        TextView rating;
        @BindView(R.id.comments)
        TextView comments;
        @BindView(R.id.timestamp)
        TextView timestamp;
        @BindView(R.id.bio)
        TextView bio;
        @BindView(R.id.avatar)
        CircularImageView avatar;
        @BindView(R.id.toggle_like)
        ToggleButton upvote;
        @BindView(R.id.iv_comments)
        ImageView iv_comments;
        @BindView(R.id.card_tag_1)
        CardView card_tag_1;
        @BindView(R.id.card_tag_2)
        CardView card_tag_2;
        @BindView(R.id.card_tag_3)
        CardView card_tag_3;
        @BindView(R.id.tv_tag_1)
        TextView tv_tag_1;
        @BindView(R.id.tv_tag_2)
        TextView tv_tag_2;
        @BindView(R.id.tv_tag_3)
        TextView tv_tag_3;

        private Event event;
        private FeedFragmentPresenter presenter;
        private boolean upvoted = false;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }

        public void bindData(Event event, FeedFragmentPresenter presenter) {
            this.event = event;
            this.presenter = presenter;
        }

        @OnClick(R.id.card)
        void setCardOnClick() {
            Context context = avatar.getContext();
            Intent i = new Intent(context, EventDetailsActivity.class);
            i.putExtra("event", Parcels.wrap(event));
            Pair<View, String> p1 = Pair.create((View)avatar, "transition_avatar");
            Pair<View, String> p2 = Pair.create((View)name, "transition_name");
            Pair<View, String> p3 = Pair.create((View)title, "transition_title");
            Pair<View, String> p4 = Pair.create((View)bio, "transition_bio");
            Pair<View, String> p5 = Pair.create((View)timestamp, "transition_timestamp");
            Pair<View, String> p6 = Pair.create((View)card, "transition_card");
            Pair<View, String> p7 = Pair.create((View)upvote, "transition_like");
            Pair<View, String> p8 = Pair.create((View)rating, "transition_rating");
            Pair<View, String> p9 = Pair.create((View)iv_comments, "transition__iv_comments");
            Pair<View, String> p10 = Pair.create((View)comments, "transition_comments");
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation((Activity) context, p1, p2, p3, p4 ,p5, p6, p7, p8, p9, p10);
            context.startActivity(i,options.toBundle());

        }

        @OnClick(R.id.avatar)
        void setAvatarOnClick() {
            Context context = avatar.getContext();
            Intent i = new Intent(context, UserProfileActivity.class);
            i.putExtra("uid",event.getOwner().getUid());
            i.putExtra("user", Parcels.wrap(event.getOwner()));
            context.startActivity(i);
            ((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_fade_out_left);
        }

        @OnClick(R.id.rating)
        void setRatingOnClick() {
            Context context = avatar.getContext();
            Intent i = new Intent(context, ViewInteractionActivity.class);
            i.putExtra("rating",true);
            i.putExtra("event", Parcels.wrap(event));
            context.startActivity(i);
        }

        @OnClick(R.id.toggle_like)
        void setUpVoteOnClick() {
            if(!upvote.isChecked()) {
                int val = Integer.valueOf(rating.getText().toString()) - 1;
                rating.setText(String.valueOf(val));
                event.setRating((long)val);
                event.setUpvotedByCurrentUser(false);
            }
            else if(upvote.isChecked()) {
                int val = Integer.valueOf(rating.getText().toString()) + 1;
                rating.setText(String.valueOf(val));
                event.setRating((long)val);
                event.setUpvotedByCurrentUser(true);
            }
            presenter.vote(event.getEventId());

        }
    }

    public FeedAdapter(Context context, FeedFragmentPresenter presenter) {
        this.context = context;
        this.presenter = presenter;

    }

    public FeedAdapter(Context context) {
        this.context = context;
    }


    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View  itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_feed, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.title.setText(eventList.get(position).getTitle());
        holder.message.setText(eventList.get(position).getMessage());
        holder.bio.setText(eventList.get(position).getOwner().getBio());
        holder.rating.setText(ValidationUtils.convertWithSuffix(eventList.get(position).getRating()));
        holder.name.setText(eventList.get(position).getOwner().getName() + " " + eventList.get(position).getOwner().getSurname());
        holder.timestamp.setText(DateUtils.getRelativeTimeSpanString(eventList.get(position).getDateCreated()));
        if(eventList.get(position).isUpvotedByCurrentUser()) {
            holder.upvote.setChecked(true);
        }
        else holder.upvote.setChecked(false);
        holder.comments.setText(ValidationUtils.convertWithSuffix(eventList.get(position).getComment_count()));


        if(eventList.get(position).getTags().size() == 1) {
            holder.tv_tag_1.setText(eventList.get(position).getTags().get(0));
            holder.card_tag_2.setVisibility(View.GONE);
            holder.tv_tag_2.setVisibility(View.GONE);
            holder.card_tag_3.setVisibility(View.GONE);
            holder.tv_tag_3.setVisibility(View.GONE);

        }
        else if(eventList.get(position).getTags().size() == 2){
            holder.tv_tag_1.setText(eventList.get(position).getTags().get(0));
            holder.tv_tag_2.setText(eventList.get(position).getTags().get(1));
            holder.card_tag_3.setVisibility(View.GONE);
            holder.tv_tag_3.setVisibility(View.GONE);

        }
        else {
            holder.tv_tag_1.setText(eventList.get(position).getTags().get(0));
            holder.tv_tag_2.setText(eventList.get(position).getTags().get(1));
            holder.tv_tag_3.setText(eventList.get(position).getTags().get(2));

        }

        Glide.with(context)
                .load(eventList.get(position).getOwner().getAvatar_url())
                .centerCrop()
                .crossFade()
                .into(holder.avatar);

        holder.bindData(eventList.get(position),presenter);
    }

    public void update(List<Event> list, int previousSize, int size) {
        this.eventList = list;
        this.size = size;
        notifyItemRangeInserted(previousSize, size);
    }

    public void notifyRemoved() {
        notifyItemRangeRemoved(0, size);
    }



    @Override
    public int getItemCount() {
        return (eventList !=null) ? eventList.size() : 0 ;
    }

}

