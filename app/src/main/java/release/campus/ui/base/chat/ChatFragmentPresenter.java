package release.campus.ui.base.chat;

import android.support.v4.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import release.campus.model.Chat;
import release.campus.model.User;
import release.campus.presenter.Presenter;

/**
 * Created by Morgoth on 15.4.2017.
 */

public class ChatFragmentPresenter extends Presenter<ChatFragmentView> {

    private DatabaseReference rootRef;
    private FirebaseUser currentUser;
    private List<Chat> chatList;

    public ChatFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database, Fragment fragment) {
        rootRef = database.getReference().child("social");
        this.currentUser = firebaseAuth.getCurrentUser();
    }

    public void getUserChats() {
        view.setRefreshing();
        DatabaseReference chatRef = rootRef.child("user_chats").child(currentUser.getUid());
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chatList.clear();
                if(view!=null && !dataSnapshot.exists()) {
                    view.onChatsLoad(chatList);
                }
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Chat c = snapshot.getValue(Chat.class);
                    chatList.add(c);
                    if(view!=null)
                        view.onChatsLoad(chatList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        chatRef.addValueEventListener(valueEventListener);
    }

    public void removeChat(Chat chat) {
        rootRef.child("user_chats").child(currentUser.getUid()).child(chat.getChatId()).removeValue();
        rootRef.child("chats").child(chat.getChatId()).child(currentUser.getUid()).removeValue();
        getUserChats();
    }

    @Override
    public void onViewAttached(ChatFragmentView view) {
        super.onViewAttached(view);
        chatList = new ArrayList<>();
    }

    @Override
    public void onViewDetached() {
        super.onViewDetached();
    }
}
