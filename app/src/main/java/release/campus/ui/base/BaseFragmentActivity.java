package release.campus.ui.base;

import android.animation.LayoutTransition;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.Transaction;
import com.google.firebase.iid.FirebaseInstanceId;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.User;
import release.campus.ui.BaseActivity;
import release.campus.ui.base.chat.ChatFragment;
import release.campus.ui.base.explore.ExploreFragment;
import release.campus.ui.base.feed.FeedFragment;
import release.campus.ui.base.profile.ProfileFragment;
import release.campus.ui.main.MainActivity;
import release.campus.ui.preferences.SettingsActivity;
import release.campus.ui.searchable.SearchableActivity;
import release.campus.utils.MyFirebaseInstanceIDService;

public class BaseFragmentActivity extends BaseActivity {

    @BindView(R.id.navigation) BottomBar bottomNavigationView;
    @BindView(R.id.toolbar) Toolbar toolbar;FragmentTransaction fragmentTransaction;
    Fragment active;
    @Inject
    FirebaseDatabase database;
    @Inject FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        setSupportActionBar(toolbar);

        final FragmentManager fragmentManager = getSupportFragmentManager();

        // define your fragments here
        final Fragment exploreFragment = new ExploreFragment();
        final Fragment feedFragment = new FeedFragment();
        final Fragment profileFragment = new ProfileFragment();
        final Fragment chatFragment = new ChatFragment();
        active = exploreFragment;

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.beginTransaction().add(R.id.placeholder, exploreFragment).commit();
        fragmentManager.beginTransaction().add(R.id.placeholder, feedFragment).hide(feedFragment).commit();
        fragmentManager.beginTransaction().add(R.id.placeholder, profileFragment).hide(profileFragment).commit();
        fragmentManager.beginTransaction().add(R.id.placeholder, chatFragment).hide(chatFragment).commit();

        bottomNavigationView.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.menu_explore:
                        fragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.fade_out)
                                .hide(active).show(exploreFragment).commit();
                        active = exploreFragment;
                        break;
                    case R.id.menu_feed:
                        fragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.fade_out)
                                .hide(active).show(feedFragment).commit();
                        active = feedFragment;
                        break;
                    case R.id.menu_profile:
                        fragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.fade_out)
                                .hide(active).show(profileFragment).commit();
                        active = profileFragment;
                       /* fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.placeholder, profileFragment).commit();*/
                        break;
                    case R.id.menu_chat:
                        fragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.fade_out)
                                .hide(active).show(chatFragment).commit();
                        active = chatFragment;
                        break;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_base_fragment, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        searchView.setQueryHint(getString(R.string.search_hint));
        searchBar.setLayoutTransition(new LayoutTransition());

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                startActivity(new Intent(BaseFragmentActivity.this, SearchableActivity.class).putExtra("query",query));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String searchQuery) {

                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.sign_out:
                handleNotificationToken();
                return true;
            case R.id.settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void handleNotificationToken() {
        final String token = FirebaseInstanceId.getInstance().getToken();
        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        final DatabaseReference eventRef = database.getReference().
                child("social").child("users").child(firebaseUser.getUid());
        eventRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                User user = mutableData.getValue(User.class);
                if (user == null) {
                    return Transaction.success(mutableData);
                }
                if(user.getNotificationTokens()!=null) {
                    if(user.getNotificationTokens().containsKey(token))
                        user.getNotificationTokens().put(token,null);
                }
                mutableData.setValue(user);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                auth.signOut();
                startActivity(new Intent(BaseFragmentActivity.this, MainActivity.class));
                BaseFragmentActivity.this.finish();
            }
        });
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.injectTo(this);
    }
}
