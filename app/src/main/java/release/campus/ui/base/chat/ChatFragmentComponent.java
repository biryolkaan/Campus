package release.campus.ui.base.chat;

import dagger.Subcomponent;
import release.campus.di.scopes.FragmentScope;

/**
 * Created by Morgoth on 15.4.2017.
 */

@FragmentScope
@Subcomponent(
        modules = {
                ChatFragmentModule.class
        }
)
public interface ChatFragmentComponent {
    void injectTo(ChatFragment fragment);
}
