package release.campus.ui.base.explore;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;


import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Event;
import release.campus.ui.BaseFragment;
import release.campus.ui.base.feed.FeedAdapter;
import release.campus.ui.base.feed.FeedFragmentPresenter;
import release.campus.ui.similar_users.SimilarUsersActivity;
import release.campus.utils.EndlessRecyclerViewScrollListener;
import release.campus.utils.FeedItemAnimator;

/**
 * Created by Morgoth on 14.3.2017.
 */

public class ExploreFragment extends BaseFragment implements ExploreFragmentView, SwipeRefreshLayout.OnRefreshListener {

    @Inject ExploreFragmentPresenter presenter;
    @Inject FeedFragmentPresenter feed_presenter;
    @Inject LinearLayoutManager linearLayoutManager;
    @Inject FirebaseAuth mAuth;
    @Inject FeedAdapter adapter;

    @BindView(R.id.rv_explore) RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.fab) FloatingActionButton fab;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fab.show();
        setupSwipeRefreshLayout();
        setupRecyclerView();
        presenter.fillFeed(false);
    }

    @OnClick(R.id.fab)
    void setSimilarFriendsClick() {
        startActivity(new Intent(this.getActivity(), SimilarUsersActivity.class));
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_fade_out_left);
    }

    @Override
    public void onDestroyView() {
        presenter.onViewDetached();
        super.onDestroyView();
    }

    void setupRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new FeedItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager,fab) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                presenter.fillFeed(false);

            }
        });


     /*   recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if (dy > 0)
                {
                    fab.hide(true);
                }
                else if (dy <0)
                    fab.show(true);
            }

        });*/
    }


    void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new ExploreFragmentModule(this)).injectTo(this);
    }

    @Override
    public void updateEvents(List<Event> eventList, int previousSize, int size) {
        swipeRefreshLayout.setRefreshing(false);
        adapter.update(eventList, previousSize, size);
    }

    @Override
    public void setRefreshing() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_explore;
    }

    @Override
    public void onRefresh() {
        adapter.notifyRemoved();
        presenter.fillFeed(true);
    }


}
