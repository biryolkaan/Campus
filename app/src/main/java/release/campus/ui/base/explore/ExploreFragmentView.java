package release.campus.ui.base.explore;

import java.util.List;

import release.campus.model.Event;

/**
 * Created by Morgoth on 14.3.2017.
 */

public interface ExploreFragmentView {
    void updateEvents(List<Event> eventList, int previousSize, int size);

    void setRefreshing();
}
