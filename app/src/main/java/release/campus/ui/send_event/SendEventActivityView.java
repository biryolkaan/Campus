package release.campus.ui.send_event;

import java.util.List;

/**
 * Created by Morgoth on 4.3.2017.
 */

public interface SendEventActivityView {

    void onSendCompleted();

    void onTagsLoad(List<String> tags);
}
