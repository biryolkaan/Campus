package release.campus.ui.send_event;

import dagger.Subcomponent;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 4.3.2017.
 */

@ActivityScope
@Subcomponent(
        modules = {
                SendEventActivityModule.class
        }
)
public interface SendEventActivityComponent {
    void injectTo(SendEventActivity activity);
}
