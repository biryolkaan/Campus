package release.campus.ui.send_event;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.ActivityModule;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 4.3.2017.
 */

@Module
public class SendEventActivityModule extends ActivityModule {

    public SendEventActivityModule(AppCompatActivity activity) {
        super(activity);
    }

    @Provides
    @ActivityScope
    public SendEventActivityPresenter provideSendEventActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        return new SendEventActivityPresenter(firebaseAuth,database);
    }
}
