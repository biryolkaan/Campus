package release.campus.ui.send_event;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import release.campus.model.Event;
import release.campus.model.Location;
import release.campus.model.Owner;
import release.campus.model.User;
import release.campus.presenter.Presenter;
import release.campus.utils.CompositeReference;
import release.campus.utils.FirebaseListenerReference;

/**
 * Created by Morgoth on 4.3.2017.
 */

public class SendEventActivityPresenter extends Presenter<SendEventActivityView> {

    private DatabaseReference rootRef;
    private FirebaseAuth mAuth;
    private CompositeReference compositeReference;
    private User u = null;
    private List<String> tags;

    public SendEventActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        this.mAuth = firebaseAuth;
        rootRef = database.getReference().child("social");
    }

    void getTags() {
        DatabaseReference tagsRef = rootRef.child("tags");
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    String value = String.valueOf(snapshot.getValue());
                    if(!value.equals("null"))
                        tags.add(value);
                    if(view!=null)
                        view.onTagsLoad(tags);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        tagsRef.addListenerForSingleValueEvent(valueEventListener);
    }

    void sendEvent(final List<String> tags, final String title, final String message, final Calendar selectedDate, final Location selectedLocation) {
        final FirebaseUser user = mAuth.getCurrentUser();
        DatabaseReference userReference = rootRef.child("users").child(user.getUid());
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                u = dataSnapshot.getValue(User.class);
                String key = rootRef.child("events").push().getKey();
                Owner owner = new Owner( u.getUid(), u.getName(), u.getSurname(), u.getBio(), u.getAvatar_url());
                Event event = new Event(key,title, tags, message , (long) 1, owner,
                        ServerValue.TIMESTAMP, selectedLocation.getLat(), selectedLocation.getLongi(), selectedLocation.getId(),
                        selectedLocation.getName(), selectedDate.getTimeInMillis());
                Map<String, Boolean> upvoters = new HashMap<>();
                upvoters.put(mAuth.getCurrentUser().getUid(),true);
                event.setUpvoters(upvoters);

                Map<String, Object> eventValues =  event.toMap();
                Map<String,Object> user_event_values = event.toMap_user_events();
                Map<String,Object> userValues = new HashMap<>();
                userValues.put(key,true);
                userValues.putAll(u.events);
                Map<String, Object> childUpdates = new HashMap<>();


               /* for (String followers: u.getFollowers().keySet()) {
                    childUpdates.put("/timeline/"  + followers
                            + "/" + eventValues.get("eventId"), true);
                }*/


                //childUpdates.put("/user_events/" + u.getUid() + "/" + key, user_event_values);
                childUpdates.put("/search/tags/" + key, eventValues);
                childUpdates.put("/events/" + key, eventValues);
                rootRef.updateChildren(childUpdates);
                view.onSendCompleted();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        compositeReference.add(new FirebaseListenerReference(userReference,listener,true));

    }

    @Override
    public void onViewAttached(SendEventActivityView view) {
        super.onViewAttached(view);
        compositeReference = new CompositeReference();
        tags = new ArrayList<>();
    }

    @Override
    public void onViewDetached() {
        compositeReference.unsubscribe();
        super.onViewDetached();
    }

}
