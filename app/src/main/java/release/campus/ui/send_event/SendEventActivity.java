package release.campus.ui.send_event;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.hootsuite.nachos.NachoTextView;
import com.hootsuite.nachos.terminator.ChipTerminatorHandler;
import com.hootsuite.nachos.validator.ChipifyingNachoValidator;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Location;
import release.campus.model.Tag;
import release.campus.ui.BaseActivity;

public class SendEventActivity extends BaseActivity implements SendEventActivityView, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final int MAP_REQUEST = 1;

    @Inject
    SendEventActivityPresenter presenter;

    @BindView(R.id.fab_send)
    FloatingActionButton fab_send;

    @BindView(R.id.tags)
    TextInputLayout tags;

    @BindView(R.id.title)
    TextInputLayout title;


    @BindView(R.id.til_date)
    TextInputLayout til_date;

    @BindView(R.id.til_location)
    TextInputLayout til_location;


    @BindView(R.id.message)
    TextInputLayout message;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.chip_tags)
    NachoTextView chip_tags;

    @BindView(R.id.root_view)
    CoordinatorLayout rootView;

    Calendar calendarDate;
    Calendar selectedDate;

    Location selectedLocation;

    @BindView(R.id.collapsing_tool_bar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.appbar)
    AppBarLayout appbar;

    private List<String> tagsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
        setContentView(R.layout.activity_send_event);

        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }

                if (scrollRange + verticalOffset == 0) {
                    //TODO animate
                    if(title.getEditText().getText().toString().isEmpty()) {
                        collapsingToolbarLayout.setTitle(getString(R.string.create_event));
                    }
                    else
                        collapsingToolbarLayout.setTitle(title.getEditText().getText().toString());
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });

        presenter.getTags();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new SendEventActivityModule(this)).injectTo(this);
    }

    @OnClick(R.id.date)
    void setDateOnClick() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                SendEventActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }


    @OnClick(R.id.location)
    void setLocationOnClick() {
        startActivityForResult(new Intent(this ,MapsActivity.class), MAP_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MAP_REQUEST) {
            if (resultCode == RESULT_OK) {
                double lat = data.getDoubleExtra("latitude",0);
                double longi = data.getDoubleExtra("longitude",0);
                String id = data.getStringExtra("id");
                String name = data.getStringExtra("name");

                selectedLocation = new Location(lat, longi, id, name);

                til_location.getEditText().setText(name);
            }
        }
    }

    @OnClick(R.id.fab_send)
    void setfabSend() {
        List<String> tagS = chip_tags.getChipValues();
        Set<String> set = new HashSet<String>(tagS);

        tagS.retainAll(tagsList);


        if(set.size() < tagS.size()){
            Toast.makeText(this, getString(R.string.error_duplicate_chip), Toast.LENGTH_SHORT).show();
            return;
        }

        if(message.getEditText().getText().toString().length() >=301) {
            Toast.makeText(this, getString(R.string.error_long_message), Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(title.getEditText().getText().toString()) || TextUtils.isEmpty(til_date.getEditText().getText().toString())
                || TextUtils.isEmpty(til_location.getEditText().getText().toString())
                || TextUtils.isEmpty(message.getEditText().getText().toString())
                || tagS.size()<=0) {
            Toast.makeText(this, getString(R.string.error_send_event), Toast.LENGTH_SHORT).show();
        }
        else {
            fab_send.setIndeterminate(true);
            String titleS = title.getEditText().getText().toString();
            String messageS = message.getEditText().getText().toString();
            presenter.sendEvent(tagS, titleS, messageS, selectedDate, selectedLocation);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }

    @Override
    public void onSendCompleted() {
        fab_send.setIndeterminate(false);
        finish();
    }

    @Override
    public void onTagsLoad(List<String> tags) {
        this.tagsList = tags;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, tags);
        chip_tags.setAdapter(adapter);
        chip_tags.addChipTerminator(' ', ChipTerminatorHandler.BEHAVIOR_CHIPIFY_TO_TERMINATOR);
        chip_tags.setNachoValidator(new ChipifyingNachoValidator());
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                SendEventActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        tpd.show(getFragmentManager(), "Timepickerdialog");

        calendarDate = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        calendarDate.getTimeInMillis();
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        selectedDate = new GregorianCalendar(calendarDate.get(Calendar.YEAR),
                calendarDate.get(Calendar.MONTH), calendarDate.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' h:mm a");
        til_date.getEditText().setText(format.format(selectedDate.getTime()));

    }
}
