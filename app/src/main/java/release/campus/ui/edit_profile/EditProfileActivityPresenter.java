package release.campus.ui.edit_profile;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import release.campus.model.Tag;
import release.campus.model.User;
import release.campus.presenter.Presenter;
import release.campus.utils.CompositeReference;
import release.campus.utils.FirebaseListenerReference;

/**
 * Created by Morgoth on 25.3.2017.
 */

public class EditProfileActivityPresenter extends Presenter<EditProvileActivityView> {

    private FirebaseUser currentUser;
    private StorageReference storageRef;
    private DatabaseReference rootRef;
    private CompositeReference compositeReference;
    private List<Tag> tagList;

    public EditProfileActivityPresenter(FirebaseAuth firebaseAuth, FirebaseStorage storage, FirebaseDatabase database) {
        this.currentUser = firebaseAuth.getCurrentUser();
        rootRef = database.getReference().child("social");
        storageRef = storage.getReference();
    }

    void init() {
        tagList = new ArrayList<>();
        DatabaseReference tagRef = rootRef.child("tags");
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Tag tag = new Tag(dataSnapshot.getKey().toString(), dataSnapshot.getValue().toString());
                tagList.add(tag);
                view.setAutoCompleteTextView(tagList);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        compositeReference.add(new FirebaseListenerReference(tagRef,childEventListener));

        DatabaseReference userRef = rootRef.child("users").child(currentUser.getUid());
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User u = dataSnapshot.getValue(User.class);
                if(view!=null)
                    view.onUserLoaded(u);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        compositeReference.add(new FirebaseListenerReference(userRef,valueEventListener,true));
    }

    void updateProfile(String name, String surname, String bio, String path, boolean isAvatarChanged, List<Tag> selected) {
        view.onProfileEdit();
        setInterests(selected);
        rootRef.child("users").child(currentUser.getUid()).child("name").setValue(name);
        rootRef.child("users").child(currentUser.getUid()).child("surname").setValue(surname);
        rootRef.child("users").child(currentUser.getUid()).child("bio").setValue(bio);
        if(isAvatarChanged)
            uploadAvatar(path);
        else {
            if(view!=null)
                view.onProfileEditComplete();
        }
    }

    void setInterests(List<Tag> selected) {
        List<String> strings = new ArrayList<>(selected.size());
        for (Object object : selected) {
            strings.add(object != null ? object.toString() : null);
        }
        Collections.sort(strings, String.CASE_INSENSITIVE_ORDER);

        String s = "";
        for (String t: strings) {
            s+=t +"_";
        }

        DatabaseReference reference = rootRef.child("users").child(currentUser.getUid()).child("interests");
        reference.setValue(s);

    }

    void uploadAvatar(String path) {

        Uri file = Uri.fromFile(new File(path));
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setContentType("image/jpeg")
                .build();

        UploadTask uploadTask = storageRef.child("avatars/"+currentUser.getUid()).putFile(file, metadata);

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @SuppressWarnings("VisibleForTests")
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                Log.e("asd","Upload is " + progress + "% done");
            }
        }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                System.out.println("Upload is paused");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @SuppressWarnings("VisibleForTests")
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // Handle successful uploads on complete
                Uri downloadUrl = taskSnapshot.getMetadata().getDownloadUrl();
                rootRef.child("users").child(currentUser.getUid()).child("avatar_url").setValue(downloadUrl.toString());
                updatePhotoUrl(downloadUrl);
                if(view!=null)
                    view.onAvatarUploadCompleted(downloadUrl);

            }
        });
    }

    void updatePhotoUrl(Uri downloadUri) {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setPhotoUri(downloadUri)
                .build();

        currentUser.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("A", "User profile updated.");
                        }
                    }
                });
    }

    @Override
    public void onViewAttached(EditProvileActivityView view) {
        super.onViewAttached(view);
        compositeReference = new CompositeReference();
    }

    @Override
    public void onViewDetached() {
        compositeReference.unsubscribe();
        super.onViewDetached();
    }
}
