package release.campus.ui.edit_profile;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.ActivityModule;
import release.campus.di.scopes.ActivityScope;
import release.campus.ui.signup.interest.InterestAdapter;

/**
 * Created by Morgoth on 25.3.2017.
 */

@Module
public class EditProfileActivityModule extends ActivityModule {

    public EditProfileActivityModule(AppCompatActivity activity) {
        super(activity);
    }

    @Provides
    @ActivityScope
    public EditProfileActivityPresenter provideEditProfileAcitivtyPresenter(FirebaseAuth firebaseAuth, FirebaseStorage storage, FirebaseDatabase database) {
        return new EditProfileActivityPresenter(firebaseAuth,storage,database);
    }

    @Provides
    @ActivityScope
    public LinearLayoutManager provideLinearLayoutManager(Context context) {
        return new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
    }

    @Provides
    @ActivityScope
    public InterestAdapter provideTagsAdapter(Context context) {
        return new InterestAdapter(context);
    }


}
