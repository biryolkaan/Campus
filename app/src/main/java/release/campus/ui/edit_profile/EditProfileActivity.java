package release.campus.ui.edit_profile;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Tag;
import release.campus.model.User;
import release.campus.ui.BaseActivity;
import release.campus.ui.signup.interest.AutoCompleteTagAdapter;
import release.campus.ui.signup.interest.InterestAdapter;
import release.campus.utils.BitmapCompress;
import release.campus.utils.SpacesItemDecoration;

public class EditProfileActivity extends BaseActivity implements EditProvileActivityView {

    private static final int REQUEST_CODE_PICKER = 0;
    private static final int RESULT_OK = -1;
    private String compressedImagePath = "";
    private boolean isAvatarChanged = false;

    @Inject EditProfileActivityPresenter presenter;
    @Inject LinearLayoutManager linearLayoutManager;

    @Inject
    InterestAdapter adapter;

    @BindView(R.id.name) TextInputLayout name;
    @BindView(R.id.surname) TextInputLayout surname;
    @BindView(R.id.bio) TextInputLayout bio;
    @BindView(R.id.profile_picture) CircularImageView profile_picture;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.rv_tags) RecyclerView recyclerView;
    @BindView(R.id.progress_overlay) FrameLayout progress_bar;
    @BindView(R.id.auto_tags)
    AutoCompleteTextView autoTags;
    private AutoCompleteTagAdapter as;
    private List<Tag> tagList;
    private boolean found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
        setContentView(R.layout.activity_edit_profile);
        setupRecyclerView();

        autoTags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                for (Tag tag : tagList) {
                    if(as.filteredList.get(i).getTag().equals(tag.getTag())){
                        Toast.makeText(EditProfileActivity.this, "You already have that.", Toast.LENGTH_SHORT).show();
                        autoTags.setText("");
                        found = true;
                        break;
                    }
                }
                if(!found) {
                    Tag tag = as.filteredList.get(i);
                    tag.setSelected(true);
                    adapter.insertItem(tag);
                    autoTags.setText("");
                }
                found = false;

            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter.init();
    }

    void setupRecyclerView() {
        StaggeredGridLayoutManager m = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        SpacesItemDecoration s = new SpacesItemDecoration(3, 10);
        recyclerView.addItemDecoration(s);
        recyclerView.setLayoutManager(m);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new EditProfileActivityModule(this)).injectTo(this);
    }

    @OnClick(R.id.profile_picture)
    void setOnClickFilePicker() {
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .limit(1) // max images can be selected (999 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    @OnClick(R.id.fab)
    void setOnClickFab() {

        if(TextUtils.isEmpty(name.getEditText().toString()) || TextUtils.isEmpty(surname.getEditText().getText().toString())
                || TextUtils.isEmpty(bio.getEditText().getText().toString()) ||
                (isAvatarChanged && TextUtils.isEmpty(compressedImagePath)) ||
                adapter.selected.size()<= 0) {
            Toast.makeText(this, getString(R.string.error_send_event), Toast.LENGTH_SHORT).show();
        }
        else {
            presenter.updateProfile(name.getEditText().getText().toString(), surname.getEditText().getText().toString(),
                    bio.getEditText().getText().toString(), compressedImagePath , isAvatarChanged, adapter.selected );
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            compressedImagePath = BitmapCompress.compressImage(images.get(0).getPath());
            isAvatarChanged = true;
            File file = new File(compressedImagePath);
            Uri imageUri = Uri.fromFile(file);
            Glide.with(this)
                    .load(imageUri)
                    .centerCrop()
                    .crossFade()
                    .into(profile_picture);
        }
    }

    @Override
    public void setAutoCompleteTextView(List<Tag> list) {
        as = new AutoCompleteTagAdapter(this, list);
        autoTags.setAdapter(as);
    }

    @Override
    public void onAvatarUploadCompleted(Uri downloadUrl) {
        Glide.with(this)
                .load(downloadUrl)
                .centerCrop()
                .crossFade()
                .into(profile_picture);
        fab.setIndeterminate(false);
    }

    @Override
    public void onUserLoaded(User u) {
        name.getEditText().setText(u.getName());
        surname.getEditText().setText(u.getSurname());
        bio.getEditText().setText(u.getBio());
        Glide.with(this)
                .load(u.getAvatar_url())
                .centerCrop()
                .crossFade()
                .into(profile_picture);

        List<Tag> tagList = new ArrayList();
        String str []= u.getInterests().split("_");;
        for (String s : str) {
            tagList.add(new Tag(s,s,true));
        }
        this.tagList = tagList;
        adapter.update(tagList);
        progress_bar.setVisibility(View.GONE);
    }

    @Override
    public void onProfileEdit() {
        fab.setIndeterminate(true);
    }

    @Override
    public void onProfileEditComplete() {
        fab.setIndeterminate(false);
        Toast.makeText(this, getString(R.string.profile_updated), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
