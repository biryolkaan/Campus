package release.campus.ui.edit_profile;

import android.net.Uri;

import java.util.List;

import release.campus.model.Tag;
import release.campus.model.User;

/**
 * Created by Morgoth on 25.3.2017.
 */

public interface EditProvileActivityView {
    void onAvatarUploadCompleted(Uri downloadUrl);

    void onUserLoaded(User u);

    void onProfileEdit();

    void onProfileEditComplete();

    void setAutoCompleteTextView(List<Tag> tagList);
}
