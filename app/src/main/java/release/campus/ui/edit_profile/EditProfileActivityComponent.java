package release.campus.ui.edit_profile;

import dagger.Subcomponent;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 25.3.2017.
 */

@ActivityScope
@Subcomponent(
        modules = {
                EditProfileActivityModule.class
        }
)
public interface EditProfileActivityComponent {
    void injectTo(EditProfileActivity activity);
}

