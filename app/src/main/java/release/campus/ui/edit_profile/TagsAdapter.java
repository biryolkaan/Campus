package release.campus.ui.edit_profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import release.campus.R;
import release.campus.model.Tag;

/**
 * Created by Morgoth on 23.5.2017.
 */


public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.ViewHolder> {

    private Context context;
    private List<Tag> tagList;

    public static class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.cb_tags)
        CheckBox tagsCheck;
        @BindView(R.id.tv_tags)
        TextView tags;

        private Tag tag;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }

        public void bindData(Tag tag) {
            this.tag = tag;
        }
    }

    public TagsAdapter(Context context) {
        this.context = context;
    }


    @Override
    public TagsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_edit_profile_tags, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tags.setText(tagList.get(position).getTag());

        holder.bindData(tagList.get(position));
    }

    public void update(List<Tag> list) {
        this.tagList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (tagList !=null) ? tagList.size() : 0 ;
    }
}
