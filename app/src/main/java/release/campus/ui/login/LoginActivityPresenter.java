package release.campus.ui.login;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

import release.campus.model.User;
import release.campus.presenter.Presenter;

/**
 * Created by Morgoth on 24.2.2017.
 */

public class LoginActivityPresenter extends Presenter<LoginActivityView> {

    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private DatabaseReference rootRef;
    Activity activity;

    public LoginActivityPresenter(FirebaseDatabase database, FirebaseAuth firebaseAuth, AppCompatActivity activity) {
        this.mAuth = firebaseAuth;
        this.activity = activity;
        this.rootRef = database.getReference().child("social");
    }

    void isSignedIn() {
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                } else {
                    // User is signed out

                }
            }
        };
    }

    void signIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if(user.isEmailVerified()) {
                                handleNotificationToken();
                            }
                            else {
                                mAuth.signOut();
                                view.onEmailVerificationError();
                            }
                        }
                        // If sign in fails, display a message to the owner. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in owner can be handled in the listener.
                        if (!task.isSuccessful()) {
                            view.onLoginError(task.getException().toString());
                        }

                    }
                });
    }

    private void handleNotificationToken() {
        final String token = FirebaseInstanceId.getInstance().getToken();
        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        final DatabaseReference eventRef = rootRef.child("users").child(firebaseUser.getUid());
        eventRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                User user = mutableData.getValue(User.class);
                if (user == null) {
                    return Transaction.success(mutableData);
                }
                if(user.getNotificationTokens()!=null) {
                    user.getNotificationTokens().put(token,true);
                }
                else {
                    Map<String,Boolean> tokens = new HashMap<>();
                    tokens.put(token,true);
                    user.setNotificationTokens(tokens);
                }
                mutableData.setValue(user);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                if(view!=null)
                    view.onSignIn();
            }
        });
    }

    @Override
    public void onViewAttached(LoginActivityView view) {
        super.onViewAttached(view);
        // mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onViewDetached() {

        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        super.onViewDetached();
    }

    public void resetPassword(String emailAddress) {
        mAuth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("LOG", "Email sent.");
                            view.onEmailSent();
                        }
                    }
                });
    }
}
