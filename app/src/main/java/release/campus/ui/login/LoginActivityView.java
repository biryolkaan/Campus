package release.campus.ui.login;

/**
 * Created by Morgoth on 24.2.2017.
 */

public interface LoginActivityView {


    void onEmailVerificationError();

    void onSignIn();

    void onEmailSent();

    void onLoginError(String s);
}
