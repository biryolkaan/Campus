package release.campus.ui.login;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.ActivityModule;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 24.2.2017.
 */
@Module
public class LoginActivityModule extends ActivityModule {

    public LoginActivityModule(AppCompatActivity activity) {
        super(activity);
    }

    @Provides
    @ActivityScope
    public LoginActivityPresenter provideLoginActivityPresenter(FirebaseDatabase database, FirebaseAuth firebaseAuth, AppCompatActivity activity) {
        return new LoginActivityPresenter(database, firebaseAuth, activity);
    }

}