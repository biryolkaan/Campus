package release.campus.ui.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.transition.TransitionManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.ui.BaseActivity;
import release.campus.ui.base.BaseFragmentActivity;
import release.campus.utils.ValidationUtils;

public class LoginActivity extends BaseActivity implements LoginActivityView {

    @Inject
    LoginActivityPresenter presenter;

    @BindView(R.id.edit_email)
    TextInputLayout email;
    @BindView(R.id.edit_password)
    TextInputLayout password;
    @BindView(R.id.linear_layout_login)
    LinearLayout linearLayout;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
        setContentView(R.layout.activity_login);
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new LoginActivityModule(this)).injectTo(this);
    }

    @OnClick(R.id.btn_signin)
    void loginOnClick() {
        if(!ValidationUtils.isValidEmail(email.getEditText().getText()))
            email.setError(getString(R.string.emailError));
        else if(password.getEditText().getText().toString().length() <= 5)
            password.setError(getString(R.string.passwordError));
        else {
            TransitionManager.beginDelayedTransition(linearLayout);
            progressBar.setVisibility(View.VISIBLE);
            presenter.signIn(email.getEditText().getText().toString(), password.getEditText().getText().toString());
        }
    }

    @Override
    protected void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }

    @Override
    public void onEmailVerificationError() {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, getString(R.string.verification_error),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSignIn() {
        TransitionManager.beginDelayedTransition(linearLayout);
        progressBar.setVisibility(View.GONE);
        startActivity(new Intent(LoginActivity.this, BaseFragmentActivity.class));
        finish();
    }

    @Override
    public void onEmailSent() {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.reset_confirmation), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void onLoginError(String s) {
        TransitionManager.beginDelayedTransition(linearLayout);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_password_reset)
    void showLocationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(getString(R.string.reset_password));
        builder.setMessage(getString(R.string.reset_password_description));
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_reset_password, null);
        builder.setView(dialogView);

        final EditText email = (EditText) dialogView.findViewById(R.id.edt_email);

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.resetPassword(email.getText().toString());
                    }
                });

        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }
}
