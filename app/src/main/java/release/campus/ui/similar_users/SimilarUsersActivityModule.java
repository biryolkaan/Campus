package release.campus.ui.similar_users;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.ActivityModule;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 21.4.2017.
 */

@Module
public class SimilarUsersActivityModule extends ActivityModule {

    public SimilarUsersActivityModule(AppCompatActivity activity) {
        super(activity);
    }

    @Provides
    @ActivityScope
    public SimilarUsersActivityPresenter provideSimilarUsersActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        return new SimilarUsersActivityPresenter(firebaseAuth,database);
    }

    @Provides
    @ActivityScope
    public SimilarUsersAdapter provideSimilarUsersAdapter(Context context) {
        return new SimilarUsersAdapter(context);
    }

    @Provides
    @ActivityScope
    public LinearLayoutManager provideLinearLayoutManager(Context context) {
        return new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
    }
}

