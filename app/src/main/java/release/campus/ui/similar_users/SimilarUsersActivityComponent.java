package release.campus.ui.similar_users;

import dagger.Subcomponent;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 21.4.2017.
 */

@ActivityScope
@Subcomponent(
        modules = {
                SimilarUsersActivityModule.class
        }
)
public interface SimilarUsersActivityComponent {
    void injectTo(SimilarUsersActivity activity);
}
