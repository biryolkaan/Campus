package release.campus.ui.similar_users;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import release.campus.R;
import release.campus.model.Owner;
import release.campus.model.User;
import release.campus.ui.user_profile.UserProfileActivity;

/**
 * Created by Morgoth on 22.4.2017.
 */

public class SimilarUsersAdapter extends RecyclerView.Adapter<SimilarUsersAdapter.ViewHolder> {

    private Context context;
    private List<User> suggestionList;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_similarity)
        TextView tv_similarity;
        @BindView(R.id.iv_avatar)
        ImageView avatar;

        private User user;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }

        public void bindData(User user) {
            this.user = user;
        }

        @OnClick(R.id.card)
        void setCardOnClick() {
            Context context = avatar.getContext();
            Intent i = new Intent(context, UserProfileActivity.class);
            i.putExtra("uid",user.getUid());
            Owner o = new Owner(user.getName(), user.getSurname(), user.getAvatar_url());
            i.putExtra("user", Parcels.wrap(o));
            context.startActivity(i);
        }
    }

    public SimilarUsersAdapter(Context context) {
        this.context = context;
    }

    @Override
    public SimilarUsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_suggestions, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tv_name.setText(suggestionList.get(position).getName() + " " +  suggestionList.get(position).getSurname());
        holder.tv_similarity.setText(String.valueOf(suggestionList.get(position).getSimilarity()));
        Glide.with(context)
                .load(suggestionList.get(position).getAvatar_url())
                .centerCrop()
                .crossFade()
                .into(holder.avatar);
        holder.bindData(suggestionList.get(position));
    }

    public void update(List<User> list) {
        this.suggestionList = list;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return (suggestionList !=null) ? suggestionList.size() : 0 ;
    }

}


