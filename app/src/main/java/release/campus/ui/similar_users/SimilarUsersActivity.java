package release.campus.ui.similar_users;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.MenuItem;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.User;
import release.campus.ui.BaseActivity;

public class SimilarUsersActivity extends BaseActivity implements SimilarUsersActivityView, SwipeRefreshLayout.OnRefreshListener  {

    @Inject SimilarUsersActivityPresenter presenter;
    @Inject SimilarUsersAdapter adapter;
    @Inject LinearLayoutManager linearLayoutManager;
    @BindView(R.id.rv_similar_users)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
        setContentView(R.layout.activity_similar_users);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupRecyclerView();
        setupSwipeRefreshLayout();
        presenter.init();
    }

    void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    void setupRecyclerView() {
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new SimilarUsersActivityModule(this)).injectTo(this);
    }

    @Override
    public void onSuggestionsLoad(List<User> suggestionList) {
        adapter.update(suggestionList);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setRefreshing() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onRefresh() {
        presenter.init();
    }
}
