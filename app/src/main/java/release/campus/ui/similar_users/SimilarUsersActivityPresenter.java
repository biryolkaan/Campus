package release.campus.ui.similar_users;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import release.campus.model.User;
import release.campus.presenter.Presenter;
import release.campus.utils.CompositeReference;
import release.campus.utils.FirebaseListenerReference;

/**
 * Created by Morgoth on 21.4.2017.
 */

public class SimilarUsersActivityPresenter extends Presenter<SimilarUsersActivityView> {

    private DatabaseReference rootRef;
    private FirebaseUser currentUser;
    private List<User> suggestionList;
    private CompositeReference compositeReference;

    private HashMap<Query, ValueEventListener> mListenerMap;

    public SimilarUsersActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        this.rootRef = database.getReference().child("social");
        this.currentUser = firebaseAuth.getCurrentUser();
    }

    public void init() {
        view.setRefreshing();
        DatabaseReference userRef = rootRef.child("users").child(currentUser.getUid());
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User u = dataSnapshot.getValue(User.class);
                String str = "";
                for (String key : u.following.keySet()) {
                    str += key +"_";
                }
                sendRequest(u,str);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        compositeReference.add(new FirebaseListenerReference(userRef,valueEventListener,true));
    }

    void getSimilarUsers() {
        Query suggestionsRef = rootRef.child("user_suggestions").child(currentUser.getUid()).orderByChild("similarity");
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                suggestionList.clear();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    User u = snapshot.getValue(User.class);
                    suggestionList.add(u);
                }
                if(view!=null)
                    view.onSuggestionsLoad(suggestionList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        suggestionsRef.addValueEventListener(valueEventListener);
        compositeReference.add(new FirebaseListenerReference(suggestionsRef,valueEventListener, true));
    }

    void sendRequest(User u,String following) {

        OkHttpClient client = new OkHttpClient();
        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://us-central1-campus-51915.cloudfunctions.net/get_similar_users").newBuilder();
        urlBuilder.addQueryParameter("interests", u.getInterests());
        urlBuilder.addQueryParameter("uid", currentUser.getUid());
        urlBuilder.addQueryParameter("following", following);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    Log.e("RESP",response.body().toString());
                    getSimilarUsers();
                }
            }
        });
    }

    @Override
    public void onViewAttached(SimilarUsersActivityView view) {
        super.onViewAttached(view);
        suggestionList = new ArrayList<>();
        compositeReference = new CompositeReference();
        mListenerMap = new HashMap<>();
    }

    @Override
    public void onViewDetached() {

        compositeReference.unsubscribe();
        super.onViewDetached();
    }
}
