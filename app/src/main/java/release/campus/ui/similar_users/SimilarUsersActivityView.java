package release.campus.ui.similar_users;

import java.util.List;

import release.campus.model.User;

/**
 * Created by Morgoth on 21.4.2017.
 */

public interface SimilarUsersActivityView {
    void onSuggestionsLoad(List<User> suggestionList);

    void setRefreshing();
}
