package release.campus.ui.searchable;

import java.util.List;

import release.campus.model.Event;

/**
 * Created by Morgoth on 30.5.2017.
 */

public interface SearchableActivityView {
    void onSearchComplete(List<Event> eventList, int previousSize, int size);

    void setRefreshing();
}
