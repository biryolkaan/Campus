package release.campus.ui.searchable;

import android.animation.LayoutTransition;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Event;
import release.campus.ui.BaseActivity;
import release.campus.ui.base.feed.FeedAdapter;

public class SearchableActivity extends BaseActivity implements SearchableActivityView, SwipeRefreshLayout.OnRefreshListener{

    @Inject SearchableActivityPresenter presenter;
    @Inject
    LinearLayoutManager linearLayoutManager;
    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @Inject
    FeedAdapter adapter;

    @BindView(R.id.rv_search_results)
    RecyclerView recyclerView;

    @BindView(R.id.empty_view)
    TextView emptyView;

    String query = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
        setContentView(R.layout.activity_searchable);
        setupSwipeRefreshLayout();
        setupRecyclerView();
        query = getIntent().getStringExtra("query");
        presenter.search(query,true);
    }

    void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }


    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new SearchableActivityModule(this)).injectTo(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_base_fragment, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        searchBar.setLayoutTransition(new LayoutTransition());
        searchItem.expandActionView();
        searchView.clearFocus();
        searchView.setQuery(query,false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.search(query,true);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String searchQuery) {

                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                finish();
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                return false;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;

    }

    @Override
    public void onSearchComplete(List<Event> eventList, int previousSize, int size) {
        if(eventList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        }
        else {
            emptyView.setVisibility(View.GONE);
            adapter.update(eventList, previousSize, size);
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setRefreshing() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onRefresh() {

    }
}
