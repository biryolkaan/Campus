package release.campus.ui.searchable;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import release.campus.model.Event;
import release.campus.presenter.Presenter;
import release.campus.utils.CompositeReference;
import release.campus.utils.FirebaseListenerReference;

/**
 * Created by Morgoth on 30.5.2017.
 */

public class SearchableActivityPresenter extends Presenter<SearchableActivityView> {

    private CompositeReference compositeReference;
    private DatabaseReference rootRef;
    private List<Event> eventList;
    private int previousSize = 0;

    public SearchableActivityPresenter(FirebaseDatabase database) {
        this.rootRef = database.getReference().child("social");
    }

    public void search(final String query, boolean reset) {
        view.setRefreshing();
        if(reset) {
            previousSize = 0;
        }
        eventList = new ArrayList<>();
        Query searchRef = rootRef.child("search").child(query);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()) {
                    if(view!=null)
                        view.onSearchComplete(eventList, previousSize, eventList.size());
                }
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Query query = rootRef.child("events").child(snapshot.getKey());
                    ValueEventListener valueEventListener1 = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Event event = dataSnapshot.getValue(Event.class);
                            eventList.add(event);
                            if(view!=null) {
                                previousSize = eventList.size();
                                view.onSearchComplete(eventList, previousSize, eventList.size());
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    };

                    compositeReference.add(new FirebaseListenerReference(query,valueEventListener1, true));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        compositeReference.add(new FirebaseListenerReference(searchRef,valueEventListener,true));

    }

    public void searchCategory(String query) {

     /*   Query search = rootRef.child("shows").orderByChild("category").startAt(query).endAt(query+"\uf8ff");
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Show show = snapshot.getValue(Show.class);
                    eventList.add(show);
                }
                view.onSearchComplete(eventList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        search.addListenerForSingleValueEvent(valueEventListener);*/

    }


    @Override
    public void onViewAttached(SearchableActivityView view) {
        super.onViewAttached(view);
        compositeReference = new CompositeReference();
    }

    @Override
    public void onViewDetached() {
        compositeReference.unsubscribe();
        super.onViewDetached();
    }
}
