package release.campus.ui.searchable;

import dagger.Subcomponent;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 30.5.2017.
 */

@ActivityScope
@Subcomponent(
        modules = {
                SearchableActivityModule.class
        }
)
public interface SearchableActivityComponent {

    void injectTo(SearchableActivity activity);

}
