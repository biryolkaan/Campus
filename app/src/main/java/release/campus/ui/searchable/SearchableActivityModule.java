package release.campus.ui.searchable;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.ActivityModule;
import release.campus.di.scopes.ActivityScope;
import release.campus.ui.base.feed.FeedAdapter;
import release.campus.ui.base.feed.FeedFragmentPresenter;

/**
 * Created by Morgoth on 30.5.2017.
 */

@Module
public class SearchableActivityModule extends ActivityModule {

    public SearchableActivityModule(AppCompatActivity activity) {
        super(activity);
    }

    @Provides
    @ActivityScope
    public SearchableActivityPresenter provideSearchableActivityPresenter(FirebaseDatabase database) {
        return new SearchableActivityPresenter(database);
    }

    @Provides
    @ActivityScope
    public FeedAdapter provideFeedAdapter(Context context, FeedFragmentPresenter presenter) {
        return new FeedAdapter(context,presenter);
    }

    @Provides
    @ActivityScope
    public FeedFragmentPresenter provideFeedFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        return new FeedFragmentPresenter(firebaseAuth,database);
    }



    @Provides
    @ActivityScope
    public LinearLayoutManager provideLinearLayoutManager(Context context) {
        return new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false);
    }

}
