package release.campus.ui.signup.interest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import release.campus.R;
import release.campus.model.Tag;

/**
 * Created by Morgoth on 28.2.2017.
 */
public class AutoCompleteTagAdapter extends ArrayAdapter<Tag> {

    private final List<Tag> tags;

    public List<Tag> filteredList = new ArrayList<>();

    public AutoCompleteTagAdapter(Context context, List<Tag> tags) {
        super(context, 0, tags);
        this.tags = tags;
    }

    @Override
    public int getCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new TagFilter(this, tags);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Tag tag = filteredList.get(position);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        convertView = inflater.inflate(R.layout.row_autocomplete, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tag);
        tvName.setText(tag.getTag());

        return convertView;
    }
}
