package release.campus.ui.signup.interest;

import dagger.Subcomponent;
import release.campus.di.scopes.FragmentScope;

/**
 * Created by Morgoth on 25.2.2017.
 */
@FragmentScope
@Subcomponent(
        modules = {
                InterestFragmentModule.class
        }
)
public interface InterestFragmentComponent {
    void injectTo(InterestFragment fragment);
}
