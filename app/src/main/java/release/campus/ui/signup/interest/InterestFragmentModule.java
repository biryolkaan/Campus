package release.campus.ui.signup.interest;

import android.support.v4.app.Fragment;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.FragmentModule;
import release.campus.di.scopes.FragmentScope;

/**
 * Created by Morgoth on 25.2.2017.
 */

@Module
public class InterestFragmentModule extends FragmentModule {

    public InterestFragmentModule(Fragment fragment) {
        super(fragment);
    }

    @Provides
    @FragmentScope
    public InterestFragmentPresenter provideInterestFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database, Fragment fragment) {
        return new InterestFragmentPresenter(firebaseAuth,database,fragment);
    }

    @Provides
    @FragmentScope
    public InterestAdapter provideInterestAdapter(Fragment fragment) {
        return new InterestAdapter(fragment.getActivity());
    }

    @Provides
    @FragmentScope
    public StaggeredGridLayoutManager provideStaggeredGridLayoutManager() {
        return  new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.HORIZONTAL);
    }

}