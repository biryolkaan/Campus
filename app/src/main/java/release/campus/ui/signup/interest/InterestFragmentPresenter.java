package release.campus.ui.signup.interest;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import release.campus.R;
import release.campus.model.Tag;
import release.campus.model.User;
import release.campus.presenter.Presenter;

/**
 * Created by Morgoth on 25.2.2017.
 */

public class InterestFragmentPresenter extends Presenter<InterestFragmentView>{

    private Activity activity;
    private FirebaseAuth mAuth;
    private DatabaseReference rootRef;
    private DatabaseReference tagReference;
    private List<Tag> tagList;

    public InterestFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database, Fragment fragment) {
        this.mAuth = firebaseAuth;
        this.activity = fragment.getActivity();
        rootRef = database.getReference().child("social");
        tagList = new ArrayList<>();
    }

    void getTags() {

        tagReference = rootRef.child("tags");

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(view!=null) {
                    view.setAdapter(tagList.subList(0,8));
                    view.setAutoCompleteTextView(tagList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("F", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };

        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Tag tag = new Tag(dataSnapshot.getKey().toString(), dataSnapshot.getValue().toString());
                tagList.add(tag);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        tagReference.addChildEventListener(childEventListener);
        tagReference.addListenerForSingleValueEvent(postListener);

    }

    void signUp(final List<Tag> selected, final User u, String password, final String school) {
        view.showProgress();
        mAuth.createUserWithEmailAndPassword(u.getEmail(), password).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //  view.onFailure(e.toString());
            }
        })
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            String token = FirebaseInstanceId.getInstance().getToken();
                            final FirebaseUser currentUser = mAuth.getCurrentUser();
                            String userID = currentUser.getUid();
                            Map<String,Boolean> following = new HashMap<String, Boolean>();
                            Map<String,Boolean> followers = new HashMap<String, Boolean>();
                            followers.put(userID,true);
                            following.put(userID,true);
                            User user = new User(u.getEmail(), u.getName(),u.getSurname(),"",0,0,userID);
                            user.setSchool(school);
                            user.setFollowing(following);
                            user.setFollowers(followers);
                            user.setEvent_count(0);
                            user.setInterests(setInterests(selected));
                            user.setAvatar_url("https://firebasestorage.googleapis.com/v0/b/campus-51915.appspot.com/o/default.png?alt=media&token=c4f56508-f327-4846-8a6b-59ec9d4a541e");
                            user.setBio("Hi!");
                            rootRef.child("users").child(userID).setValue(user);
                            Map<String,Boolean> tokens = new HashMap<>();
                            tokens.put(token,true);
                            user.setNotificationTokens(tokens);
                            setInterests(selected);
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(u.getName() + " " + u.getSurname())
                                    .setPhotoUri(Uri.parse(user.getAvatar_url()))
                                    .build();

                            currentUser.updateProfile(profileUpdates)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d("A", "User profile updated.");

                                                currentUser.sendEmailVerification();
                                                Toast.makeText(activity, activity.getString(R.string.verify),
                                                        Toast.LENGTH_LONG).show();
                                                view.hideProgress();
                                                mAuth.signOut();
                                            }
                                        }
                                    });

                        }
                        // If sign in fails, display a message to the owner. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in owner can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(activity, "Failed to sign in.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    String setInterests(List<Tag> selected) {
        //TODO test çıkıp geri gir
        List<String> strings = new ArrayList<>(selected.size());
        for (Object object : selected) {
            strings.add(object != null ? object.toString() : null);
        }
        Collections.sort(strings, String.CASE_INSENSITIVE_ORDER);

        String s = "";
        for (String t: strings) {
            s+=t +"_";
        }
        return s;
    }

    @Override
    public void onViewDetached() {
        super.onViewDetached();
    }

    @Override
    public void onViewAttached(InterestFragmentView view) {
        super.onViewAttached(view);
    }
}
