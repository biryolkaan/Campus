package release.campus.ui.signup;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.User;
import release.campus.ui.BaseFragment;
import release.campus.ui.login.LoginActivity;
import release.campus.utils.NonSwipeableViewPager;
import release.campus.utils.ValidationUtils;

/**
 * Created by Morgoth on 24.2.2017.
 */

public class SignUpFragment extends BaseFragment implements SignUpFragmentView {

    @Inject SignUpFragmentPresenter presenter;

    @BindView(R.id.edit_email) TextInputLayout email;
    @BindView(R.id.edit_password) TextInputLayout password;
    @BindView(R.id.name) TextInputLayout name;
    @BindView(R.id.surname) TextInputLayout surname;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.edt_school)
    EditText school;
    String[] items;
    @BindView(R.id.activity_sign_up)
    LinearLayout linearLayout;

    NonSwipeableViewPager pager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
    }

    OnSignUpDataListener mOnSignUpDataListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mOnSignUpDataListener = (OnSignUpDataListener)context;
        }catch (ClassCastException e){

        }

    }

    int selectedIndex = 0;

    @OnClick(R.id.edt_school)
    void setSchoolOnClick() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.title_school);

        items = getResources().getStringArray(R.array.schools);
        builder.setSingleChoiceItems(items, selectedIndex,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedIndex = which;
                        school.setText(items[which]);

                    }
                });

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(selectedIndex==0) {
                            school.setText(items[0]);
                        }
                    }
                });

        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(school.getText().toString().isEmpty()) {
                        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.reminder_school), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        }
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();

    }

    public interface OnSignUpDataListener {
        public void onDataReceived(User user, String password, String school);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_signup, container,
                false);
        pager = ButterKnife.findById(getActivity(), R.id.viewPager);
        return rootView;
    }

    @OnClick(R.id.btn_login)
    void setLoginClick() {
        startActivity(new Intent(this.getActivity(), LoginActivity.class));
    }

    @OnClick(R.id.btn_next)
    void setSignup() {

        if(!ValidationUtils.isValidEmail(email.getEditText().getText()))
            email.setError(getString(R.string.emailError));
        else if(!ValidationUtils.isValidPassword(password.getEditText().getText().toString()))
            password.setError(getString(R.string.passwordError));
        else if(!ValidationUtils.isValidName(name.getEditText().getText().toString()))
            name.setError(getString(R.string.nameError));
        else if(!ValidationUtils.isValidName(surname.getEditText().getText().toString()))
            surname.setError(getString(R.string.surnameError));
        else {
        /*presenter.signUp(email.getEditText().getText().toString(), password.getEditText().getText().toString(),
                name.getEditText().getText().toString(),surname.getEditText().getText().toString());*/
            User u = new User(email.getEditText().getText().toString(),
                    name.getEditText().getText().toString(),surname.getEditText().getText().toString());
            mOnSignUpDataListener.onDataReceived(u, password.getEditText().getText().toString(), school.getText().toString());
            pager.setCurrentItem(2,true);
        }
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new SignUpFragmentModule(this)).injectTo(this);
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    public void onDestroyView() {
        presenter.onViewDetached();
        super.onDestroyView();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onFailure(String s) {
        Toast.makeText(this.getContext(), s,
                Toast.LENGTH_SHORT).show();
    }
}