package release.campus.ui.signup;

/**
 * Created by Morgoth on 24.2.2017.
 */

public interface SignUpFragmentView {
    void showProgress();

    void hideProgress();

    void onFailure(String s);
}
