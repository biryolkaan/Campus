package release.campus.ui.signup.interest;

import java.util.List;

import release.campus.model.Tag;

/**
 * Created by Morgoth on 25.2.2017.
 */

public interface InterestFragmentView {
    void setAutoCompleteTextView(List<Tag> tagList);
    void setAdapter(List<Tag> tagList);

    void showProgress();
    void hideProgress();
}
