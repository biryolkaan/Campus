package release.campus.ui.signup;

import android.support.v4.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.FragmentModule;
import release.campus.di.scopes.FragmentScope;

/**
 * Created by Morgoth on 24.2.2017.
 */

@Module
public class SignUpFragmentModule extends FragmentModule {

    public SignUpFragmentModule(Fragment fragment) {
        super(fragment);
    }

    @Provides
    @FragmentScope
    public SignUpFragmentPresenter provideSignUpFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database, Fragment fragment) {
        return new SignUpFragmentPresenter(firebaseAuth,database,fragment);
    }

}