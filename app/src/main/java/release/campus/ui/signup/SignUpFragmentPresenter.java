package release.campus.ui.signup;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import release.campus.R;
import release.campus.presenter.Presenter;
import release.campus.model.User;

/**
 * Created by Morgoth on 24.2.2017.
 */

public class SignUpFragmentPresenter extends Presenter<SignUpFragmentView> {

    private Activity activity;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;

    public SignUpFragmentPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database, Fragment fragment) {
        this.mAuth = firebaseAuth;
        this.activity = fragment.getActivity();
        this.database = database;
        databaseReference = database.getReference().child("social");
    }


    void signUp(final String email, String password, final String name, final String surname) {
        view.showProgress();
        mAuth.createUserWithEmailAndPassword(email, password).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                view.onFailure(e.toString());
            }
        })
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            FirebaseUser currentUser = mAuth.getCurrentUser();
                            String userID = currentUser.getUid();
                            User user = new User(email, name,surname,"",0,0,userID);
                            databaseReference.child("social").child("users").child(userID).setValue(user);
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(name + " " + surname)
                                    .build();

                            currentUser.updateProfile(profileUpdates)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d("A", "User profile updated.");
                                            }
                                        }
                                    });

                            currentUser.sendEmailVerification();

                            Toast.makeText(activity, activity.getString(R.string.verify),
                                    Toast.LENGTH_LONG).show();
                            view.hideProgress();
                        }
                        // If sign in fails, display a message to the owner. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in owner can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(activity, "Failed to sign in.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    @Override
    public void onViewAttached(SignUpFragmentView view) {
        super.onViewAttached(view);
    }

    @Override
    public void onViewDetached() {
        super.onViewDetached();
    }

}
