package release.campus.ui.signup.interest;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import release.campus.model.Tag;

/**
 * Created by Morgoth on 28.2.2017.
 */

class TagFilter extends Filter {

    AutoCompleteTagAdapter adapter;
    private List<Tag> originalList;
    List<Tag> filteredList;

    public TagFilter(AutoCompleteTagAdapter adapter, List<Tag> originalList) {
        super();
        this.adapter = adapter;
        this.originalList = originalList;
        this.filteredList = new ArrayList<>();
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        filteredList.clear();
        final FilterResults results = new FilterResults();

        if (constraint == null || constraint.length() == 0) {
            filteredList.addAll(originalList);
        } else {
            final String filterPattern = constraint.toString().toLowerCase().trim();

            for (final Tag tag : originalList) {
                if (tag.getTag().contains(filterPattern)) {
                    filteredList.add(tag);
                }
            }
        }
        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.filteredList.clear();
        adapter.filteredList.addAll((List) results.values);
        adapter.notifyDataSetChanged();
    }
}