package release.campus.ui.signup.interest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.badoualy.stepperindicator.StepperIndicator;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Tag;
import release.campus.model.User;
import release.campus.ui.BaseFragment;
import release.campus.ui.signup.SignUpFragment;
import release.campus.utils.SpacesItemDecoration;

/**
 * Created by Morgoth on 24.2.2017.
 */

public class InterestFragment extends BaseFragment implements InterestFragmentView, SignUpFragment.OnSignUpDataListener {

    @Inject InterestFragmentPresenter presenter;
    @Inject InterestAdapter adapter;
    @Inject StaggeredGridLayoutManager staggeredGridLayoutManager;

    @BindView(R.id.auto_tags) AutoCompleteTextView autoTags;
    @BindView(R.id.recycler_interests) RecyclerView recyclerView;
    @BindView(R.id.progress_bar) ProgressBar progress_bar;

    StepperIndicator indicator;
    private List<Tag> tagList;
    private AutoCompleteTagAdapter as;
    private User user;
    private String password;
    private String school;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //https://github.com/JakeWharton/butterknife/issues/102
        autoTags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(!tagList.contains(as.filteredList.get(i))) {
                    adapter.insertItem(as.filteredList.get(i));
                    autoTags.setText("");
                }
                else {
                    Toast.makeText(getActivity(), "You already have that.", Toast.LENGTH_SHORT).show();
                    autoTags.setText("");
                }
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("ONCREATE","ACT");
        presenter.onViewAttached(this);
        presenter.getTags();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_interests, container,
                false);
        indicator = ButterKnife.findById(getActivity(), R.id.indicator);
        return rootView;
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new InterestFragmentModule(this)).injectTo(this);
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    public void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }

    @Override
    public void setAutoCompleteTextView(List<Tag> list) {
        as = new AutoCompleteTagAdapter(getActivity(), list);
        autoTags.setAdapter(as);
    }

    @OnClick(R.id.done)
    void setDoneOnClick() {
        if(adapter.selected.size()>=3 && user!=null && password!=null) {
            presenter.signUp(adapter.selected, user, password, school);
        }
        else Toast.makeText(getActivity(), getString(R.string.interestError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress_bar.setVisibility(View.GONE);
        indicator.setCurrentStep(2);
        Toast.makeText(getActivity(), getString(R.string.signupComplete), Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    @Override
    public void setAdapter(List<Tag> tagList) {
        this.tagList = tagList;
        StaggeredGridLayoutManager m = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        SpacesItemDecoration s = new SpacesItemDecoration(3, 10);
        recyclerView.addItemDecoration(s);
        recyclerView.setLayoutManager(m);
        recyclerView.setAdapter(adapter);
        adapter.update(tagList);
    }

    @Override
    public void onDataReceived(User user,String password, String school) {
        Log.e("TAG", "Finally ! received data");
        this.user = user;
        this.password = password;
        this.school = school;
    }
}
