package release.campus.ui.signup;

import dagger.Subcomponent;
import release.campus.di.scopes.FragmentScope;

/**
 * Created by Morgoth on 24.2.2017.
 */

@FragmentScope
@Subcomponent(
        modules = {
                SignUpFragmentModule.class
        }
)
public interface SignUpFragmentComponent {
    void injectTo(SignUpFragment fragment);
}
