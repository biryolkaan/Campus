package release.campus.ui.signup;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.os.Bundle;
import android.util.Log;

import com.badoualy.stepperindicator.StepperIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.User;
import release.campus.ui.BaseActivity;
import release.campus.ui.signup.interest.InterestFragment;
import release.campus.utils.NonSwipeableViewPager;

public class SignUpActivity extends BaseActivity implements SignUpFragment.OnSignUpDataListener {

    @BindView(R.id.viewPager) NonSwipeableViewPager pager;

    CustomPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        pager.setPagingEnabled(false);
        adapter = new CustomPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SignUpFragment(), "ONE");
        adapter.addFragment(new InterestFragment(), "TWO");
        pager.setAdapter(adapter);
        StepperIndicator indicator = (StepperIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(pager);

    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {

    }

    @Override
    public void onDataReceived(User user, String password, String school) {
        Log.e("TAG", "data received to Activity... send to view pager");
        adapter.onDataReceived(user, password, school);
    }

    private class CustomPagerAdapter extends FragmentStatePagerAdapter implements SignUpFragment.OnSignUpDataListener {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        public CustomPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public void onDataReceived(User user, String password, String school) {
            Log.e("TAG", "data received to view pager... sending to fragment2");
            InterestFragment articleFrag = (InterestFragment)
                    mFragmentList.get(1);
            articleFrag.onDataReceived(user,password,school);
        }
    }

}
