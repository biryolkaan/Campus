package release.campus.ui.signup.interest;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import release.campus.R;
import release.campus.model.Tag;

/**
 * Created by Morgoth on 25.2.2017.
 */

public class InterestAdapter extends RecyclerView.Adapter<InterestAdapter.ViewHolder> {

    private Context context;
    private List<Tag> tagList;
    //TODO static fix
    public static List<Tag> selected;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.row_card)
        CardView card;
        @BindView(R.id.tv_interest)
        TextView tv_interest;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
            colors = new int[2];
            colors[0] = ContextCompat.getColor(view.getContext(), R.color.jagger);
            colors[1] = ContextCompat.getColor(view.getContext(),R.color.btnColorHighlight);
        }

        private Tag tag;
       // public List<Tag> selected;
        private int[] colors;

        public void bindData(Tag tag) {
            this.tag = tag;
        }

        @OnClick(R.id.row_card)
        void setBtn_tag() {
            if(selected.contains(tag)) {
                cardAnim(card, true);
                selected.remove(tag);
            }
            else {
                cardAnim(card,false);
                selected.add(tag);
            }
        }

        void cardAnim(final CardView view, boolean isSelected) {
            int colorFrom;
            int colorTo;
            if (isSelected) {
                colorFrom = colors[1];
                colorTo = colors[0];
            } else {
                colorFrom = colors[0];
                colorTo = colors[1];
            }
            ValueAnimator colorAnim = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
            colorAnim.setDuration(400);
            colorAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    ((CardView) view).setCardBackgroundColor((int) animation.getAnimatedValue());
                }
            });
            colorAnim.start();

        }

    }

    public InterestAdapter(Context context) {
        this.context = context;
        selected = new ArrayList<>();
    }

    @Override
    public InterestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_interest, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tv_interest.setText(tagList.get(position).getTag());

        if(tagList.get(position).isSelected()) {
            holder.cardAnim(holder.card, false);
            selected.add(tagList.get(position));
        }

        holder.bindData(tagList.get(position));


    }

    public void update(List<Tag> tagList) {
        this.tagList = tagList;
        notifyDataSetChanged();
    }

    public void insertItem(Tag tag) {
        tagList.add(tag);
        notifyItemInserted(getItemCount());

    }

    @Override
    public int getItemCount() {
        return (tagList !=null) ? tagList.size() : 0 ;
    }

}