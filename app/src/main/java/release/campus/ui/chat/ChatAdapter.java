package release.campus.ui.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import release.campus.R;
import release.campus.model.Message;

/**
 * Created by Morgoth on 15.4.2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private final int NOTMYMESSAGE = 0, MYMESSAGE = 1;

    private Context context;
    private List<Message> messageList;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.message)
        TextView tv_message;

        private Message message;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }

        public void bindData(Message message) {
            this.message = message;

        }
    }

    public ChatAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ChatAdapter.ViewHolder viewHolder;

        LayoutInflater itemView = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case MYMESSAGE:
                View view = itemView.inflate(R.layout.row_message, parent, false);
                viewHolder = new ViewHolder(view);
                break;
            case NOTMYMESSAGE:
                View view1 = itemView.inflate(R.layout.row_their_message, parent, false);
                viewHolder = new ViewHolder(view1);
                break;
            default:
                View view2 = itemView.inflate(android.R.layout.simple_list_item_1, parent, false);
                viewHolder = new ViewHolder(view2);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tv_message.setText(messageList.get(position).getMessage());
       /* Glide.with(context)
                .load(commentList.get(position).getOwner().getAvatar_url())
                .centerCrop()
                .crossFade()
                .into(holder.avatar);*/
        holder.bindData(messageList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        if (messageList.get(position).isMyMessage()) {
            return MYMESSAGE;
        } else if (!messageList.get(position).isMyMessage()) {
            return NOTMYMESSAGE;
        }
        return -1;
    }

    public void update(List<Message> list, int previousSize, int size) {
        this.messageList = list;
        notifyItemRangeInserted(previousSize,size);
    }

    @Override
    public int getItemCount() {
        return (messageList !=null) ? messageList.size() : 0 ;
    }

}

