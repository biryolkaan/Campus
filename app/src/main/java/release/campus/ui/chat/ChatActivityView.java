package release.campus.ui.chat;

import java.util.List;

import release.campus.model.Message;

/**
 * Created by Morgoth on 15.4.2017.
 */

public interface ChatActivityView {
    void onMessagedLoad(List<Message> messageList, int previousSize, int size);

    void setRefreshing();

    void enableChat();
}
