package release.campus.ui.chat;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.EditText;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import release.campus.R;
import release.campus.di.components.ApplicationComponent;
import release.campus.model.Chat;
import release.campus.model.Message;
import release.campus.model.Owner;
import release.campus.ui.BaseActivity;

public class ChatActivity extends BaseActivity implements ChatActivityView, SwipeRefreshLayout.OnRefreshListener{

    @Inject ChatActivityPresenter presenter;
    @Inject ChatAdapter adapter;
    @BindView(R.id.rv_chat)
    RecyclerView recyclerView;
    @BindView(R.id.editText_message)
    EditText message;
    @BindView(R.id.btn_sendMessage)
    FloatingActionButton sendMessage;

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;

    private Owner chatReceiver;
    private Chat chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewAttached(this);
        setContentView(R.layout.activity_chat);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sendMessage.setEnabled(false);
        setupRecyclerView();
        setupSwipeRefreshLayout();
        chatReceiver = (Owner) Parcels.unwrap(getIntent().getParcelableExtra("receiver"));
        chat = (Chat) Parcels.unwrap(getIntent().getParcelableExtra("chat"));
        getSupportActionBar().setTitle(chatReceiver.getName() + " "+ chatReceiver.getSurname());
        presenter.init(chatReceiver,chat);
    }

    void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
        swipeRefreshLayout.setOnRefreshListener(this);
    }


    private void setupRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        manager.setStackFromEnd(true);
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.btn_sendMessage)
    void setSendMessge() {
        Message m = new Message(chatReceiver.getUid(),message.getText().toString());
        presenter.sendMessage(m, chatReceiver);
        message.getText().clear();
    }

    @OnTextChanged(R.id.editText_message)
    void setOnTextChanged(CharSequence c) {
        if(c.length() > 0)
            sendMessage.setEnabled(true);
        else sendMessage.setEnabled(false);

    }

    @Override
    protected void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.plus(new ChatActivityModule(this)).injectTo(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMessagedLoad(List<Message> messageList, int previousSize, int size) {
        adapter.update(messageList, previousSize, size);
        recyclerView.smoothScrollToPosition(size);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setEnabled(false);
    }

    @Override
    public void setRefreshing() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void enableChat() {
        message.setEnabled(true);
        sendMessage.setClickable(true);
    }

    @Override
    public void onRefresh() {

    }
}
