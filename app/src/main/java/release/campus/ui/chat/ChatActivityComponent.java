package release.campus.ui.chat;

import dagger.Subcomponent;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 15.4.2017.
 */

@ActivityScope
@Subcomponent(
        modules = {
                ChatActivityModule.class
        }
)
public interface ChatActivityComponent {
    void injectTo(ChatActivity activity);
}

