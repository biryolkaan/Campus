package release.campus.ui.chat;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;
import release.campus.di.modules.ActivityModule;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 15.4.2017.
 */

@Module
public class ChatActivityModule extends ActivityModule {

    public ChatActivityModule(AppCompatActivity activity) {
        super(activity);
    }

    @Provides
    @ActivityScope
    public ChatActivityPresenter provideChatActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        return new ChatActivityPresenter(firebaseAuth,database);
    }

    @Provides
    @ActivityScope
    public ChatAdapter provideChatAdapter(Context context) {
        return new ChatAdapter(context);
    }
}