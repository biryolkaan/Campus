package release.campus.ui.chat;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import release.campus.model.Chat;
import release.campus.model.Message;
import release.campus.model.Owner;
import release.campus.model.User;
import release.campus.presenter.Presenter;
import release.campus.utils.CompositeReference;
import release.campus.utils.FirebaseListenerReference;

/**
 * Created by Morgoth on 15.4.2017.
 */

public class ChatActivityPresenter  extends Presenter<ChatActivityView>{

    private DatabaseReference rootRef;
    private List<Message> messageList;
    private FirebaseUser currentUser;
    private CompositeReference compositeReference;
    private DatabaseReference chatRef;
    private String chatId;
    private Owner receiver;
    private boolean chatFoundOnServer = false;
    private int previousSize = 0;

    public ChatActivityPresenter(FirebaseAuth firebaseAuth, FirebaseDatabase database) {
        rootRef = database.getReference().child("social");
        this.currentUser = firebaseAuth.getCurrentUser();
    }

    public void init(final Owner receiver, Chat chat) {
        this.receiver = receiver;
        if(chat!=null && chat.getChatId()!=null && !chat.getChatId().isEmpty()) {
            this.chatId = chat.getChatId();
            view.enableChat();
        }
        else {
            checkForChat();
        }

        getMessage();
    }

    void checkForChat() {
        view.setRefreshing();
        chatRef = rootRef.child("user_chats").child(currentUser.getUid());

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Chat c = snapshot.getValue(Chat.class);
                        if(c!=null && c.getChatReceiverId()!=null && c.getChatReceiverId().equals(receiver.getUid())) {
                            chatId = snapshot.getKey();
                            getMessage();
                            view.enableChat();
                        }
                    }
                }
                else {
                    checkForDeletedChat(receiver);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        chatRef.addListenerForSingleValueEvent(valueEventListener);
    }

    private void checkForDeletedChat(final Owner receiver) {
        final DatabaseReference chatRef2 = rootRef.child("user_chats").child(receiver.getUid());
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()) {
                    view.enableChat();
                }
                if(dataSnapshot.hasChildren()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Chat c = snapshot.getValue(Chat.class);
                        if(c!=null && c.getChatReceiverId()!=null && c.getChatReceiverId().equals(currentUser.getUid())) {
                            chatId = snapshot.getKey();
                            chatFoundOnServer = true;
                            getMessage();
                            view.enableChat();
                        }
                    }
                    view.enableChat();
                }
                view.enableChat();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                view.enableChat();
            }
        };

        chatRef2.addListenerForSingleValueEvent(valueEventListener);
    }

    void sendMessage(final Message m, final Owner receiver) {

        if(chatId == null) {

            chatId = chatRef.push().getKey();

            Chat chat = new Chat(chatId, currentUser.getUid(), receiver.getUid(),
                    currentUser.getDisplayName(),
                    (receiver.getName() + " " + receiver.getSurname()),
                    currentUser.getPhotoUrl().toString(),
                    receiver.getAvatar_url());

            Chat chat2 = new Chat(chatId, receiver.getUid(), currentUser.getUid(),
                    (receiver.getName() + " " + receiver.getSurname()),
                    currentUser.getDisplayName(),
                    receiver.getAvatar_url().toString(),
                    currentUser.getPhotoUrl().toString());

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("/user_chats/" +  currentUser.getUid() + "/" + chatId, chat.toMap());
            childUpdates.put("/user_chats/" +  receiver.getUid()+ "/" + chatId, chat2.toMap());
            rootRef.updateChildren(childUpdates);
            getMessage();
         /*   Chat chat = new Chat(chatId, currentUser.getUid(), receiver.getUid(),
                    currentUser.getDisplayName(),
                    (receiver.getName() + " " + receiver.getSurname()),
                    currentUser.getPhotoUrl().toString(),
                    receiver.getAvatar_url());
            chatRef.child(chatId).setValue(chat);*/

        }

        else if(chatId!=null)
        {

            Chat chat = new Chat(chatId, currentUser.getUid(), receiver.getUid(),
                    currentUser.getDisplayName(),
                    (receiver.getName() + " " + receiver.getSurname()),
                    currentUser.getPhotoUrl().toString(),
                    receiver.getAvatar_url());

            Chat chat2 = new Chat(chatId, receiver.getUid(), currentUser.getUid(),
                    (receiver.getName() + " " + receiver.getSurname()),
                    currentUser.getDisplayName(),
                    receiver.getAvatar_url().toString(),
                    currentUser.getPhotoUrl().toString());

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("/user_chats/" +  currentUser.getUid() + "/" + chatId, chat.toMap());
            childUpdates.put("/user_chats/" +  receiver.getUid()+ "/" + chatId, chat2.toMap());
            rootRef.updateChildren(childUpdates);

            //chatFoundOnServer = false;
        }

        DatabaseReference userRef = rootRef.child("users").child(currentUser.getUid());
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User u = dataSnapshot.getValue(User.class);
                Owner o = new Owner(u.getUid(),u.getName(), u.getSurname(), u.getAvatar_url());
                m.setOwner(o);

                String messageId = rootRef.child("chats").child(chatId).child(currentUser.getUid()).push().getKey();

                Map<String, Object> postValues = m.toMap();

                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("/chats/" + chatId + "/" + currentUser.getUid() + "/" + messageId, postValues);
                childUpdates.put("/chats/" + chatId + "/" + receiver.getUid() + "/" + messageId, postValues);

                rootRef.updateChildren(childUpdates);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        userRef.addListenerForSingleValueEvent(valueEventListener);

    }

    void getMessage() {
        view.setRefreshing();
        if(chatId!=null) {
            DatabaseReference messageRef = rootRef.child("chats").child(chatId).child(currentUser.getUid());
            ValueEventListener valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    messageList.clear();
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                        Message m = snapshot.getValue(Message.class);
                        if(m.getOwner().getUid().equals(currentUser.getUid()))
                            m.setMyMessage(true);
                        messageList.add(m);
                    }

                    if(view!=null) {
                        previousSize = messageList.size();
                        view.onMessagedLoad(messageList, previousSize, messageList.size());
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            compositeReference.add(new FirebaseListenerReference(messageRef,valueEventListener,false));
        }
        else {
            if(view!=null)
            view.onMessagedLoad(messageList, previousSize, messageList.size());
        }
    }

    @Override
    public void onViewAttached(ChatActivityView view) {
        super.onViewAttached(view);
        messageList = new ArrayList<>();
        compositeReference = new CompositeReference();
    }

    @Override
    public void onViewDetached() {
        compositeReference.unsubscribe();
        super.onViewDetached();
    }


}
