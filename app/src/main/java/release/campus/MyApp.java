package release.campus;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import release.campus.di.components.ApplicationComponent;
import release.campus.di.components.DaggerApplicationComponent;
import release.campus.di.modules.ApplicationModule;

/**
 * Created by Morgoth on 23.2.2017.
 */

public class MyApp extends Application {

    private ApplicationComponent component;

    @NonNull
    public static ApplicationComponent getComponent(Context context) {
        return ((MyApp) context.getApplicationContext()).getComponent();
    }

    @NonNull
    public ApplicationComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        this.component = prepareApplicationComponent().build();
        this.component.injectTo(this);

    }

    @NonNull
    protected DaggerApplicationComponent.Builder prepareApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this));
    }
}