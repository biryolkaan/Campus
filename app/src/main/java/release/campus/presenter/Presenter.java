package release.campus.presenter;

/**
 * Created by Morgoth on 24.2.2017.
 */

public abstract class Presenter<V> {

    protected V view;

    public void onViewAttached(V view) {
        this.view = view;
    }

    public void onViewDetached() {
        this.view = null;
    }

    protected boolean isViewAttached() {
        return view != null;
    }

}