package release.campus.utils;

import android.animation.Animator;
import android.view.View;
import android.view.ViewAnimationUtils;

/**
 * Created by Morgoth on 29.4.2017.
 */

public class AnimUtils {

    public static void revealActivity(View viewToAnimate) {

        int cx = viewToAnimate.getRight();
        int cy = viewToAnimate.getBottom();

        int endRadius = (int) Math.hypot(viewToAnimate.getWidth(), viewToAnimate.getHeight());


        Animator circularReveal = ViewAnimationUtils.createCircularReveal(viewToAnimate, cx, cy, 0, endRadius);
       // circularReveal.setDuration(300);

        // make the view visible and start the animation
        viewToAnimate.setVisibility(View.VISIBLE);
        circularReveal.start();
    }

    public static void revealFab(View viewToAnimate) {

        int cx = (viewToAnimate.getLeft() + viewToAnimate.getRight()) / 2;
        int cy = viewToAnimate.getTop();
        int finalRadius = Math.max(viewToAnimate.getWidth(), viewToAnimate.getHeight());



      //  int cx = viewToAnimate.getHeight() / 2;
     //   int cy = viewToAnimate.getWidth() / 2 ;

        int endRadius = (int) Math.hypot(viewToAnimate.getWidth(), viewToAnimate.getHeight());


        Animator circularReveal = ViewAnimationUtils.createCircularReveal(viewToAnimate, cx, cy, 0, finalRadius);
        // circularReveal.setDuration(300);

        // make the view visible and start the animation
        viewToAnimate.setVisibility(View.VISIBLE);
        circularReveal.start();
    }
}
