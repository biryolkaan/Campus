package release.campus.utils;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Morgoth on 9.3.2017.
 */

public class FirebaseListenerReference {

    private ChildEventListener childEventListener;
    private ValueEventListener valueEventListener;
    private Query reference;

    public FirebaseListenerReference(Query reference, ValueEventListener valueEventListener, boolean isSingle) {
        this.reference = reference;
        this.valueEventListener = valueEventListener;
        if(isSingle)
            reference.addListenerForSingleValueEvent(valueEventListener);
        else
            reference.addValueEventListener(valueEventListener);
    }

    public FirebaseListenerReference(DatabaseReference reference, ChildEventListener childEventListener) {
        this.reference = reference;
        this.childEventListener = childEventListener;
        reference.addChildEventListener(childEventListener);
    }

    public FirebaseListenerReference(Query query, ChildEventListener childEventListener) {
        this.reference = query;
        this.childEventListener = childEventListener;
        reference.addChildEventListener(childEventListener);
    }

    public void detach() {
        if(valueEventListener!=null) {
            reference.removeEventListener(valueEventListener);
        }
        else if(childEventListener!=null)
            reference.removeEventListener(childEventListener);
    }
}
