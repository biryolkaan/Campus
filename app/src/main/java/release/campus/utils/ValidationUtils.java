package release.campus.utils;

import android.text.TextUtils;

import java.util.List;

/**
 * Created by Morgoth on 25.2.2017.
 */

public class ValidationUtils {

    private static final String NAMES[] = new String[]{
            "K",
            "M",
            "B",
            "T",
    };

    public static boolean isEmpty(CharSequence target) {
        return !TextUtils.isEmpty(target.toString().trim());
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidPassword(CharSequence target) {
        return !TextUtils.isEmpty(target) && target.length()>=6;
    }

    public static boolean isValidName(CharSequence target) {
        return !TextUtils.isEmpty(target) && target.length()>=2;
    }

    public static boolean isValidTag(List<String> tagList) {
        return tagList.size()>0 && tagList.size()<4;
    }

    public static String convertWithSuffix(long count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format("%.1f%c",
                count / Math.pow(1000, exp),
                "KMGTPE".charAt(exp-1));
    }
}
