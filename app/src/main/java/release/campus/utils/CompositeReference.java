package release.campus.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Morgoth on 9.3.2017.
 */

public class CompositeReference {

    private List<FirebaseListenerReference> referenceList;

    public CompositeReference() {
        referenceList = new ArrayList<>();
    }

    public void add(FirebaseListenerReference reference) {
        referenceList.add(reference);
    }

    public void unsubscribe() {
        for (FirebaseListenerReference ref: referenceList) {
            ref.detach();
        }
    }
}
