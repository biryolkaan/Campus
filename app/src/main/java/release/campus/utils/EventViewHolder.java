package release.campus.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import release.campus.R;
import release.campus.model.Event;
import release.campus.ui.base.feed.FeedFragmentPresenter;
import release.campus.ui.event_details.EventDetailsActivity;
import release.campus.ui.user_profile.UserProfileActivity;
import release.campus.ui.user_profile.view_interaction.ViewInteractionActivity;

/**
 * Created by Morgoth on 30.4.2017.
 */

public class EventViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.name)
    public
    TextView name;
    @BindView(R.id.title)
    public
    TextView title;
    @BindView(R.id.message)
    public
    TextView message;
    @BindView(R.id.rating)
    public
    TextView rating;
    @BindView(R.id.comments)
    TextView comments;
    @BindView(R.id.timestamp)
    public
    TextView timestamp;
    @BindView(R.id.bio)
    public
    TextView bio;
    @BindView(R.id.avatar)
    public
    CircularImageView avatar;
    @BindView(R.id.toggle_like)
    public
    ToggleButton upvote;
    @BindView(R.id.iv_comments)
    ImageView iv_comments;
    @BindView(R.id.card_tag_1)
    CardView card_tag_1;
    @BindView(R.id.card_tag_2)
    CardView card_tag_2;
    @BindView(R.id.card_tag_3)
    CardView card_tag_3;
    @BindView(R.id.tv_tag_1)
    TextView tv_tag_1;
    @BindView(R.id.tv_tag_2)
    TextView tv_tag_2;
    @BindView(R.id.tv_tag_3)
    TextView tv_tag_3;

    private Event event;
    private FeedFragmentPresenter presenter;
    private boolean upvoted = false;

    public EventViewHolder(View view) {
        super(view);
        ButterKnife.bind(this,view);
    }

    public void bindData(Event event, FeedFragmentPresenter presenter) {
        this.event = event;
        this.presenter = presenter;
    }

    @OnClick(R.id.card)
    void setCardOnClick() {
        Context context = avatar.getContext();
        Intent i = new Intent(context, EventDetailsActivity.class);
        i.putExtra("event", Parcels.wrap(event));
        Pair<View, String> p1 = Pair.create((View)avatar, "transition_avatar");
        Pair<View, String> p2 = Pair.create((View)name, "transition_name");
        Pair<View, String> p3 = Pair.create((View)title, "transition_title");
        Pair<View, String> p4 = Pair.create((View)bio, "transition_bio");
        Pair<View, String> p5 = Pair.create((View)timestamp, "transition_timestamp");
        Pair<View, String> p6 = Pair.create((View)card, "transition_card");
        Pair<View, String> p7 = Pair.create((View)upvote, "transition_like");
        Pair<View, String> p8 = Pair.create((View)rating, "transition_rating");
        Pair<View, String> p9 = Pair.create((View)iv_comments, "transition__iv_comments");
        Pair<View, String> p10 = Pair.create((View)comments, "transition_comments");
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation((Activity) context, p1, p2, p3, p4 ,p5, p6, p7, p8, p9, p10);

        context.startActivity(i,options.toBundle());
    }

    @OnClick(R.id.avatar)
    void setAvatarOnClick() {
        Context context = avatar.getContext();
        Intent i = new Intent(context, UserProfileActivity.class);
        i.putExtra("uid",event.getOwner().getUid());
        i.putExtra("user", Parcels.wrap(event.getOwner()));
        context.startActivity(i);

    }

    @OnClick(R.id.rating)
    void setRatingOnClick() {
        Context context = avatar.getContext();
        Intent i = new Intent(context, ViewInteractionActivity.class);
        i.putExtra("rating",true);
        i.putExtra("event", Parcels.wrap(event));
        context.startActivity(i);
    }

    @OnClick(R.id.toggle_like)
    void setUpVoteOnClick() {
        presenter.vote(event.getEventId());
    }

    public void populateHolder(Context context, Event model, String uid, FeedFragmentPresenter presenter) {
        title.setText(model.getTitle());
        message.setText(model.getMessage());
        bio.setText(model.getOwner().getBio());
        rating.setText(String.valueOf(model.getRating()));
        name.setText(model.getOwner().getName() + " " + model.getOwner().getSurname());
        timestamp.setText(DateUtils.getRelativeTimeSpanString(model.getDateCreated()));
        comments.setText(String.valueOf(model.getComment_count()));
        if(model.upvoters != null && model.upvoters.containsKey(uid)) {
            model.setUpvotedByCurrentUser(true);
            upvote.setChecked(true);
        }
        else upvote.setChecked(false);

        if(model.getTags().size() == 1) {
            tv_tag_1.setText(model.getTags().get(0));
            card_tag_2.setVisibility(View.GONE);
            tv_tag_2.setVisibility(View.GONE);
            card_tag_3.setVisibility(View.GONE);
            tv_tag_3.setVisibility(View.GONE);

        }
        else if(model.getTags().size() == 2){
            tv_tag_1.setText(model.getTags().get(0));
            tv_tag_2.setText(model.getTags().get(1));
            card_tag_3.setVisibility(View.GONE);
            tv_tag_3.setVisibility(View.GONE);

        }
        else {
            tv_tag_1.setText(model.getTags().get(0));
            tv_tag_2.setText(model.getTags().get(1));
            tv_tag_3.setText(model.getTags().get(2));

        }

        Glide.with(context)
                .load(model.getOwner().getAvatar_url())
                .centerCrop()
                .crossFade()
                .into(avatar);

        bindData(model,presenter);
    }
}
