package release.campus.di.components;

import javax.inject.Singleton;

import dagger.Component;
import release.campus.MyApp;
import release.campus.di.modules.ApplicationModule;
import release.campus.ui.base.BaseFragmentActivity;
import release.campus.ui.base.chat.ChatFragmentComponent;
import release.campus.ui.base.chat.ChatFragmentModule;
import release.campus.ui.base.explore.ExploreFragmentComponent;
import release.campus.ui.base.explore.ExploreFragmentModule;
import release.campus.ui.base.feed.FeedFragmentComponent;
import release.campus.ui.base.feed.FeedFragmentModule;
import release.campus.ui.base.profile.ProfileFragmentComponent;
import release.campus.ui.base.profile.ProfileFragmentModule;
import release.campus.ui.chat.ChatActivityComponent;
import release.campus.ui.chat.ChatActivityModule;
import release.campus.ui.edit_profile.EditProfileActivityComponent;
import release.campus.ui.edit_profile.EditProfileActivityModule;
import release.campus.ui.event_details.EventDetailsActivityComponent;
import release.campus.ui.event_details.EventDetailsActivityModule;
import release.campus.ui.login.LoginActivityComponent;
import release.campus.ui.login.LoginActivityModule;
import release.campus.ui.searchable.SearchableActivityComponent;
import release.campus.ui.searchable.SearchableActivityModule;
import release.campus.ui.send_event.SendEventActivityComponent;
import release.campus.ui.send_event.SendEventActivityModule;
import release.campus.ui.signup.SignUpFragmentComponent;
import release.campus.ui.signup.SignUpFragmentModule;
import release.campus.ui.signup.interest.InterestFragmentComponent;
import release.campus.ui.signup.interest.InterestFragmentModule;
import release.campus.ui.similar_users.SimilarUsersActivityComponent;
import release.campus.ui.similar_users.SimilarUsersActivityModule;
import release.campus.ui.user_profile.UserProfileActivityComponent;
import release.campus.ui.user_profile.UserProfileActivityModule;
import release.campus.ui.user_profile.view_interaction.ViewInteractionActivityComponent;
import release.campus.ui.user_profile.view_interaction.ViewInteractionActivityModule;

/**
 * Created by Morgoth on 23.2.2017.
 */

@Singleton
@Component(
        modules = {
                ApplicationModule.class
        }
)

public interface ApplicationComponent {
    void injectTo(MyApp injectTo);
    void injectTo(BaseFragmentActivity fragmentActivity);
    SendEventActivityComponent plus(SendEventActivityModule activityModule);
    FeedFragmentComponent plus(FeedFragmentModule fragmentModule);
    ProfileFragmentComponent plus(ProfileFragmentModule fragmentModule);
    SignUpFragmentComponent plus(SignUpFragmentModule fragmentModule);
    InterestFragmentComponent plus(InterestFragmentModule fragmentModule);
    ExploreFragmentComponent plus(ExploreFragmentModule fragmentModule);
    LoginActivityComponent plus(LoginActivityModule activityModule);
    UserProfileActivityComponent plus(UserProfileActivityModule activityModule);
    EditProfileActivityComponent plus(EditProfileActivityModule activityModule);
    EventDetailsActivityComponent plus(EventDetailsActivityModule eventDetailsActivityModule);
    ChatActivityComponent plus(ChatActivityModule chatActivityModule);
    ChatFragmentComponent plus(ChatFragmentModule chatFragmentModule);
    SimilarUsersActivityComponent plus(SimilarUsersActivityModule similarUsersActivityModule);
    ViewInteractionActivityComponent plus(ViewInteractionActivityModule viewInteractionActivityModule);

    SearchableActivityComponent plus(SearchableActivityModule searchableActivityModule);
}