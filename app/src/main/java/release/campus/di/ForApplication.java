package release.campus.di;

import javax.inject.Qualifier;

/**
 * Created by Morgoth on 23.2.2017.
 */

@Qualifier
public @interface ForApplication {
}