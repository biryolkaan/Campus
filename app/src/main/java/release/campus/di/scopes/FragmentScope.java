package release.campus.di.scopes;

import javax.inject.Scope;

/**
 * Created by Morgoth on 25.2.2017.
 */
@Scope
public @interface FragmentScope {
}