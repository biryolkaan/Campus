package release.campus.di.scopes;

import javax.inject.Scope;

/**
 * Created by Morgoth on 23.2.2017.
 */

@Scope
public @interface ActivityScope {
}