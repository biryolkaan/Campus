package release.campus.di.modules;

import android.content.Context;
import android.support.v4.app.Fragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Morgoth on 25.2.2017.
 */

@Module
public abstract class FragmentModule {

    private final Fragment fragment;

    public FragmentModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    public Fragment provideFragment() {
        return fragment;
    }

    @Provides
    public Context provideContext() {
        return fragment.getContext();
    }
}