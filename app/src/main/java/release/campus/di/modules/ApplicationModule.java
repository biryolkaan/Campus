package release.campus.di.modules;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import release.campus.MyApp;
import release.campus.di.ForApplication;

/**
 * Created by Morgoth on 23.2.2017.
 */

@Module
public class ApplicationModule {

    @NonNull
    private final MyApp app;

    public ApplicationModule(@NonNull MyApp app) {
        this.app = app;
    }

    @Provides
    @Singleton
    FirebaseAuth provideFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }

    @Provides
    @Singleton
    FirebaseDatabase provideFirebaseDatabase() {
        return  FirebaseDatabase.getInstance();
    }

    @Provides
    @Singleton
    FirebaseStorage provideFirebaseStorage() {
        return  FirebaseStorage.getInstance();
    }

    @NonNull
    @Provides
    @Singleton
    public static Resources provideResources(@NonNull @ForApplication Context context) {
        return context.getResources();
    }

    @NonNull
    @Provides
    @Singleton
    public Application provideApplication() {
        return app;
    }

    @NonNull
    @Provides
    @Singleton
    @ForApplication
    public Context provideApplicationContext() {
        return app.getApplicationContext();
    }

}
