package release.campus.di.modules;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import release.campus.di.scopes.ActivityScope;

/**
 * Created by Morgoth on 23.2.2017.
 */

@Module
public abstract class ActivityModule {

    private final AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    public AppCompatActivity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    public Context provideContext() {
        return activity.getBaseContext();
    }
}